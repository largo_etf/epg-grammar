#pragma once

#include "ShapeOperation.h"

namespace SplitGrammar
{
	class HalfOp : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	};
}