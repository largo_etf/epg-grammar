#pragma once

#include "ShapeOperation.h"
#include <string>

class Material;

namespace SplitGrammar
{
	class GenerationContext;
	
	class InstanceOperation : public ShapeOperation
	{
		//static Material *defaultMat;
		static std::string curr_material;
		static bool initialized;

		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();

		InstanceOperation();

		friend class OperationFactory;
		friend class TexOp;
		friend class ColOp;
		friend class MatOp;
	};
}