#pragma once

#include "ShapeOperation.h"
#include <map>
#include <string>
namespace SplitGrammar
{
	class Subdiv : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();
	};
}