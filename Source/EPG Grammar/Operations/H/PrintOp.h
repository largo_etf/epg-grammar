#pragma once

#include "ShapeOperation.h"

namespace SplitGrammar
{
	class PrintOp : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();
	};
}