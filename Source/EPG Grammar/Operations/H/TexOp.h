#pragma once

#include "ShapeOperation.h"

namespace SplitGrammar
{
	class TexOp : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();
	};

	class ColOp : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();
	};

	class MatOp : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();
	};
}