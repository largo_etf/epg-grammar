#pragma once

#include "ShapeOperation.h"
#include <glm/mat4x4.hpp>
#include "Shape.h"

namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	class Symbol;

	class BasicSplit : public ShapeOperation
	{
		Symbol *symbol;

		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();

	public:

		friend yy::parser;
	};
}