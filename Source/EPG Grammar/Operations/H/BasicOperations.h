#pragma once

#include "ShapeOperation.h"
#include "Symbol.h"

namespace SplitGrammar
{
	class TransformationOperation : public ShapeOperation
	{
	protected:
		bool is_local;
		Code oc;
	public:
		TransformationOperation(Code oc, bool is_local);
	};
	class TranslateOperation : public TransformationOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	public:
		TranslateOperation(Code oc, bool is_local);
	};

	class RotateOperation : public TransformationOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	public:
		RotateOperation(Code oc, bool is_local);
	};

	class ScaleOperation : public TransformationOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	public:
		ScaleOperation(Code oc, bool is_local);
	};

	class PushOperation : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	};

	class PopOperation : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	};

	class Epsilon : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	};

	class DebugScope : public ShapeOperation
	{
		virtual void Operation(float args[MAX_ARGS], GenerationContext* context);
		virtual ShapeOperation *InstancePrototype();
	};
}