#pragma once

#include "ShapeOperation.h"

namespace SplitGrammar
{
	class CompOp : public ShapeOperation
	{
		static std::map<std::string, int> split_type_map;
		static bool initialized;

		CompOp();

		virtual void Operation(float args[MAX_ARGS], GenerationContext *context);
		virtual ShapeOperation *InstancePrototype();

		friend class OperationFactory;
	};
}