#include "TexOp.h"
#include "Grammar.h"
#include "InstanceOperation.h"
#include "DSZMaterial.h"
#include "PNGTexture.h"
#include <string>

using namespace SplitGrammar;
using namespace std;

ShapeOperation *TexOp::InstancePrototype()
{
	return new TexOp();
}

void TexOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	//InstanceOperation::defaultMat = new DSZMaterial(&SimpleShader);
	DSZMaterial *mat = (DSZMaterial*)(context->grammarModel->GetMaterial(InstanceOperation::curr_material));
	
	string name = "..\\resources\\" + string(str_arg);
	Texture *tex = Texture::GetTexture(name.c_str());
	if (tex == 0)
	{
		tex = new PNGTexture();
		tex->Load(name.c_str());
	}

	mat->Texture = tex;
	mat->UseTexture();
	mat->UseLighting = true;
}

ShapeOperation *ColOp::InstancePrototype()
{
	return new ColOp();
}

void ColOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	//InstanceOperation::defaultMat = new DSZMaterial(&SimpleShader);
	DSZMaterial *mat = (DSZMaterial*)(context->grammarModel->GetMaterial(InstanceOperation::curr_material));

	mat->color = glm::vec4(args[0], args[1], args[2], args[3]);

	mat->UseStaticColor();
	mat->UseLighting = true;
}

ShapeOperation *MatOp::InstancePrototype()
{
	return new MatOp();
}

void MatOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	Material *mat = context->grammarModel->GetMaterial(str_arg);
	if (!mat)
	{
		mat = new DSZMaterial(&SimpleShader);
		context->grammarModel->SaveMaterial(mat, str_arg);
	}

	InstanceOperation::curr_material = str_arg;
}
