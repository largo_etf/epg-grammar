#include "BasicOperations.h"
#include "Grammar.h"
#include "DSZMathHeader.h"
#include "Shape.h"
#include "DSZMath.h"
#include "Model.h"

using namespace SplitGrammar;
using namespace glm;

TransformationOperation::TransformationOperation(Code oc, bool is_local) : oc(oc), is_local(is_local) { }


TranslateOperation::TranslateOperation(Code oc, bool is_local) : TransformationOperation(oc, is_local) { }

void TranslateOperation::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	GeomAttrib &attrib = context->currShape->geom_attrib;
	GeomAttrib &orig = context->currShape->original_attrib;
	GeomAttrib &origin = context->currShape->shape_origin_attrib;

	mat4 mat = axis_to_mat(attrib.x, attrib.y, attrib.z);
	mat4 mat_orig = axis_to_mat(orig.x, orig.y, orig.z);
	mat4 mat_origin = axis_to_mat(origin.x, origin.y, origin.z);

	vec4 tr = vec4(args[0], args[1], args[2], 1.0f);
	vec4 pos = vec4(attrib.position, 1.0f);
	vec4 pos_orig = vec4(orig.position, 1.0f);
	vec4 pos_origin = vec4(origin.position, 1.0f);
	
	if (!is_local)
	{
		context->currShape->geom_attrib.position = (vec3)((pos + mat * tr) * vec4((oc == ADD)) +
			(pos - mat * tr) * vec4((oc == SUB)) +
			(pos_orig + mat_orig * tr) * vec4((oc == ASSIGN)));
	}
	else
	{
		context->currShape->shape_origin_attrib.position = (vec3)((pos_origin - mat_origin * tr) * vec4((oc == ADD)) +
			(pos_origin + mat_origin * tr) * vec4((oc == SUB)) +
			(tr) * vec4((oc == ASSIGN)));
	}
}

ShapeOperation *TranslateOperation::InstancePrototype()
{
	return new TranslateOperation(oc, is_local);
}


RotateOperation::RotateOperation(Code oc, bool is_local) : TransformationOperation(oc, is_local) { }

void RotateOperation::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	//vec3 rot = vec3(args[0], args[1], args[2]);
	GeomAttrib &attrib = context->currShape->geom_attrib;
	GeomAttrib &origin = context->currShape->original_attrib;
	mat4 rot, rot_neg;

	rot *= rotate(args[0], vec3(1, 0, 0));
	rot *= rotate(args[1], vec3(0, 1, 0));
	rot *= rotate(args[2], vec3(0, 0, 1));

	rot_neg *= rotate(-args[0], vec3(1, 0, 0));
	rot_neg *= rotate(-args[1], vec3(0, 1, 0));
	rot_neg *= rotate(-args[2], vec3(0, 0, 1));

	if (!is_local)
	{
		attrib.x = (vec3)((vec4(attrib.x, 1) * rot_neg) * vec4((oc == ADD)) +
			(vec4(attrib.x, 1) * rot) * vec4((oc == SUB)) +
			(vec4(origin.x, 1) * rot_neg) * vec4((oc == ASSIGN)));
		attrib.y = (vec3)((vec4(attrib.y, 1) * rot_neg) * vec4((oc == ADD)) +
			(vec4(attrib.y, 1) * rot) * vec4((oc == SUB)) +
			(vec4(origin.y, 1) * rot_neg) * vec4((oc == ASSIGN)));
		attrib.z = (vec3)((vec4(attrib.z, 1) * rot_neg) * vec4((oc == ADD)) +
			(vec4(attrib.z, 1) * rot) * vec4((oc == SUB)) +
			(vec4(origin.z, 1) * rot_neg) * vec4((oc == ASSIGN)));
	}
	else
	{
		context->currShape->shape_origin_attrib.x = (vec3)((vec4(attrib.x, 1) * rot) * vec4((oc == ADD)) +
			(vec4(attrib.x, 1) * rot_neg) * vec4((oc == SUB)) +
			(vec4(origin.x, 1) * rot) * vec4((oc == ASSIGN)));
		context->currShape->shape_origin_attrib.y = (vec3)((vec4(attrib.y, 1) * rot) * vec4((oc == ADD)) +
			(vec4(attrib.y, 1) * rot_neg) * vec4((oc == SUB)) +
			(vec4(origin.y, 1) * rot) * vec4((oc == ASSIGN)));
		context->currShape->shape_origin_attrib.z = (vec3)((vec4(attrib.z, 1) * rot) * vec4((oc == ADD)) +
			(vec4(attrib.z, 1) * rot_neg) * vec4((oc == SUB)) +
			(vec4(origin.z, 1) * rot) * vec4((oc == ASSIGN)));
	}
}

ShapeOperation *RotateOperation::InstancePrototype()
{
	return new RotateOperation(oc, is_local);
}


ScaleOperation::ScaleOperation(Code oc, bool is_local) : TransformationOperation(oc, is_local) { }

void ScaleOperation::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	vec3 sc(args[0], args[1], args[2]);
	vec3 scdiv;
	// divide zero guard
	scdiv.x = sc.x + 1.0f * (sc.x == 0.0 && oc != DIV);
	scdiv.y = sc.y + 1.0f * (sc.y == 0.0 && oc != DIV);
	scdiv.z = sc.z + 1.0f * (sc.z == 0.0 && oc != DIV);

	if (!is_local)
	{
		vec3 &curr = context->currShape->geom_attrib.size;
		context->currShape->geom_attrib.size = (curr + sc) * vec3((oc == ADD)) +
			(curr - sc) * vec3((oc == SUB)) +
			(curr * sc) * vec3((oc == MUL)) +
			(curr / scdiv) * vec3((oc == DIV)) +
			sc * vec3((oc == ASSIGN));
	}
	else
	{
		vec3 &curr = context->currShape->geom_attrib.size;
		context->currShape->shape_origin_attrib.size = 1.0f / ((curr + sc) * vec3((oc == ADD)) +
			(curr - sc) * vec3((oc == SUB)) +
			(curr * sc) * vec3((oc == MUL)) +
			(curr / sc) * vec3((oc == DIV)) +
			sc * vec3((oc == ASSIGN)));
	}
}

ShapeOperation *ScaleOperation::InstancePrototype()
{
	return new ScaleOperation(oc, is_local);
}


void PushOperation::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	context->currShape->Push();
}

ShapeOperation *PushOperation::InstancePrototype()
{
	return new PushOperation();
}

void PopOperation::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	context->currShape->Pop();
}

ShapeOperation *PopOperation::InstancePrototype()
{
	return new PopOperation();
}

void Epsilon::Operation(float args[MAX_ARGS], GenerationContext* context)
{

}

ShapeOperation *Epsilon::InstancePrototype()
{
	return new Epsilon();
}

void DebugScope::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	context->currShape->AddModel(Shape::box);
}

ShapeOperation *DebugScope::InstancePrototype()
{
	return new DebugScope();
}