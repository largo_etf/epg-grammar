#include "Subdiv.h"
#include "Grammar.h"
#include "DSZMathHeader.h"
#include "DSZMath.h"

using namespace glm;
using namespace SplitGrammar;

void Subdiv::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	float abs_sum = 0;
	float rel_sum = 0;
	float dim = 0;

	GeomAttrib my_attrib = (inherit_attrib) ? context->currShape->geom_attrib : this->geom_attrib;

	if (str_arg[0] == 'X' || str_arg[0] == 'x')
		dim = my_attrib.size.x;
	else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
		dim = my_attrib.size.y;
	else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
		dim = my_attrib.size.z;

	for (int i = 0; i < subsplits_cnt; ++i)
	{
		abs_sum += args[i] * !rel_map[i];
		rel_sum += args[i] * rel_map[i];
	}

	float r = (dim - abs_sum) / rel_sum;
	float last_dim = 0;

	for (int i = 0; i < subsplits_cnt; ++i)
	{
		split_ops[i]->inherit_attrib = false;
		float new_dim = args[i];
		if (rel_map[i] == 1)
			new_dim *= r;
		GeomAttrib attrib = GeomAttrib(my_attrib);
		
		if (str_arg[0] == 'X' || str_arg[0] == 'x')
		{
			attrib.size.x = new_dim;
			attrib.position += (vec3)(axis_to_mat(attrib.x, attrib.y, attrib.z) * (last_dim * vec4(1, 0, 0, 1)));
		}
		else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
		{
			attrib.size.y = new_dim;
			attrib.position += (vec3)(axis_to_mat(attrib.x, attrib.y, attrib.z) * (last_dim * vec4(0, 1, 0, 1)));
		}
		else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
		{
			attrib.size.z = new_dim;
			attrib.position += (vec3)(axis_to_mat(attrib.x, attrib.y, attrib.z) * (last_dim * vec4(0, 0, 1, 1)));
		}
	
		split_ops[i]->geom_attrib = attrib;

		split_ops[i]->op(context->curr_node->args, context);

		last_dim += new_dim;
	}
}

ShapeOperation *Subdiv::InstancePrototype()
{
	return new Subdiv();
}