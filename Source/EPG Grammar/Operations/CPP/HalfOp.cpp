#include "HalfOp.h"
#include "Grammar.h"
#include <glm\vec3.hpp>
#include <glm\vec4.hpp>
#include "DSZMath.h"

using namespace glm;
using namespace SplitGrammar;

ShapeOperation *HalfOp::InstancePrototype()
{
	return new HalfOp();
}

void HalfOp::Operation(float args[MAX_ARGS], GenerationContext* context)
{
	GeomAttrib my_attrib = (inherit_attrib) ? context->currShape->geom_attrib : this->geom_attrib;

	float size;
	if (str_arg[0] == 'X' || str_arg[0] == 'x')
		size = my_attrib.size.x;
	else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
		size = my_attrib.size.y;
	else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
		size = my_attrib.size.z;

	for (int i = 0; i < 2; ++i)
	{
		split_ops[i]->inherit_attrib = false;
		GeomAttrib new_attrib = my_attrib;

		if (str_arg[0] == 'X' || str_arg[0] == 'x')
		{
			new_attrib.size.x = size / 2;
			new_attrib.position += (vec3)(axis_to_mat(my_attrib.x, my_attrib.y, my_attrib.z) * (i * size / 2 * vec4(1, 0, 0, 1)));
		}
		else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
		{
			new_attrib.size.y = size / 2;
			new_attrib.position += (vec3)(axis_to_mat(my_attrib.x, my_attrib.y, my_attrib.z) * (i * size / 2 * vec4(0, 1, 0, 1)));
		}
		else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
		{
			new_attrib.size.z = size / 2;
			new_attrib.position += (vec3)(axis_to_mat(my_attrib.x, my_attrib.y, my_attrib.z) * (i * size / 2 * vec4(0, 0, 1, 1)));
		}

		split_ops[i]->geom_attrib = new_attrib;
		split_ops[i]->op(context->curr_node->args, context);
	}
}