#include "BasicSplit.h"
#include "Grammar.h"
#include "Shape.h"
#include "Symbol.h"
#define GLM_FORCE_RADIANS
#include <glm\mat4x4.hpp>
#include <glm\ext.hpp>

using namespace glm;
using namespace SplitGrammar;

void BasicSplit::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	Production *pr = context->grammar->chooseProduction(symbol, args, context);
	if (pr != 0)
	{
		GenerationContext::QueueNode *new_node = new GenerationContext::QueueNode();
		new_node->pr = pr;

		memcpy(new_node->args, args, MAX_ARGS * sizeof(float));

		if (str_arg)
		{
			new_node->str_arg = new char[strlen(str_arg) + 1];
			strcpy(new_node->str_arg, str_arg);
		}

		Shape *shape = new Shape();
		shape->geom_attrib = (inherit_attrib)? context->currShape->geom_attrib : this->geom_attrib;
		shape->geom_attrib = context->currShape->GetTotalAttrib(shape->geom_attrib);
		shape->original_attrib = shape->geom_attrib;

		//shape->origin_tr = context->currShape->GetTransform(shape->origin_attrib);

		shape->symbol = symbol;
		new_node->node = context->grammarModel->tree->AddChild(shape);
		context->queue.push_back(new_node);
	}
}

ShapeOperation *BasicSplit::InstancePrototype()
{
	return new BasicSplit();
}

int a()
{
	return 0;
}