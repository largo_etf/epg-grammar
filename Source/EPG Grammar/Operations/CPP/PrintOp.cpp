#include "PrintOp.h"
#include <iostream>

using namespace SplitGrammar;

void PrintOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	std::cout << args[0] << std::endl;
}

ShapeOperation *PrintOp::InstancePrototype()
{
	return new PrintOp();
}