#include "Repeat.h"
#include "Grammar.h"
#include <glm/mat4x4.hpp>
#include <glm\ext.hpp>
#include "DSZMath.h"

using namespace SplitGrammar;
using namespace glm;

ShapeOperation *RepeatOp::InstancePrototype()
{
	return new RepeatOp();
}

void RepeatOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	float piece_size = args[0];
	float scope_size;

	GeomAttrib my_attrib = (inherit_attrib) ? context->currShape->geom_attrib : this->geom_attrib;

	if (str_arg[0] == 'X' || str_arg[0] == 'x')
		scope_size = my_attrib.size.x;
	else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
		scope_size = my_attrib.size.y;
	else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
		scope_size = my_attrib.size.z;

	bool snap = str_arg[strlen(str_arg) - 2] == 's' || str_arg[strlen(str_arg) - 1] == 's';
	bool fill = str_arg[strlen(str_arg) - 2] == 'f' || str_arg[strlen(str_arg) - 1] == 'f';

	int num_pieces = (int)(scope_size / piece_size);
	float remainder = scope_size - num_pieces * piece_size;
	float spacing = remainder / (num_pieces);

	if (num_pieces != 0)
	{
		for (int i = 0; i < num_pieces; ++i)
		{
			GeomAttrib new_attrib = GeomAttrib(my_attrib);
			GeomAttrib fill_attrib = GeomAttrib(my_attrib);
			float new_size = piece_size + spacing * snap;
			float trans = i * piece_size + spacing * i;


			if (str_arg[0] == 'X' || str_arg[0] == 'x')
			{
				new_attrib.size.x = new_size;
				new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (trans * vec4(1, 0, 0, 1)));

				fill_attrib.size.x = spacing;
				fill_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * ((trans + new_size) * vec4(1, 0, 0, 1)));
			}
			else if (str_arg[0] == 'Y' || str_arg[0] == 'y')
			{
				new_attrib.size.y = new_size;
				new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (trans * vec4(0, 1, 0, 1)));

				fill_attrib.size.y = spacing;
				fill_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * ((trans + new_size) * vec4(0, 1, 0, 1)));
			}
			else if (str_arg[0] == 'Z' || str_arg[0] == 'z')
			{
				new_attrib.size.z = new_size;
				new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (trans * vec4(0, 0, 1, 1)));

				fill_attrib.size.z = spacing;
			}

			split_ops[0]->inherit_attrib = false;
			split_ops[0]->geom_attrib = new_attrib;

			split_ops[0]->op(context->curr_node->args, context);

			if (fill)
			{
				split_ops[1]->inherit_attrib = false;
				split_ops[1]->geom_attrib = fill_attrib;

				split_ops[1]->op(context->curr_node->args, context);
			}
		}
	}
	else
	{
		if (fill)
		{
			GeomAttrib fill_attrib = GeomAttrib(my_attrib);

			split_ops[1]->inherit_attrib = false;
			split_ops[1]->geom_attrib = fill_attrib;

			split_ops[1]->op(context->curr_node->args, context);
		}
	}
}