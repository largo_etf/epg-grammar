#include "InstanceOperation.h"
#include "Grammar.h"
#include "Model.h"
#include "Mesh.h"
#include <string>
#include "Shape.h"
#include "DSZMaterial.h"
#include "Globals.h"
#include "GeometryBatch.h"

using namespace glm;
using namespace std;
using namespace SplitGrammar;

string InstanceOperation::curr_material = "default_mat";
bool InstanceOperation::initialized = false;

InstanceOperation::InstanceOperation()
{
	//if (!initialized)
	//{
	//	DSZMaterial* mat = new DSZMaterial(&SimpleShader);
	//	Material::SaveMaterial(mat, curr_material);
	//	mat->color = vec4(0.5f, 0.5f, 0.5f, 1.0f);
	//	mat->UseStaticColor();
	//	mat->UseLighting = true;

	//	initialized = true;
	//}
}

void InstanceOperation::Operation(float args[MAX_ARGS], SplitGrammar::GenerationContext *context)
{
	Model *model;
	string name = "..\\resources\\" + string(str_arg);

	if (!strcmp(str_arg, "Cube"))
	{
		Mesh *mesh = new Mesh();
		mesh->GenerateCube();
		model = new Model();
		model->AddMesh(mesh);
	}
	else
	{
		model = Model::GetModel(name.c_str());
	}
	
	//model->SetAllMeshesMaterial(Material::GetMaterial();
	context->currShape->AddModel(model);
	BatchKey key(curr_material, model);
	context->grammarModel->batch->AddInstance(key, context->currShape->GetTransform());
}

ShapeOperation *InstanceOperation::InstancePrototype()
{
	return new InstanceOperation();
}