#include "CompOp.h"
#include "Grammar.h"
#include "DSZMathHeader.h"
#include "Shape.h"
#include "DSZMath.h"

using namespace SplitGrammar;
using namespace glm;

std::map<std::string, int> CompOp::split_type_map;
bool CompOp::initialized = false;

#define FACE_0 0x1
#define FACE_1 0x2
#define FACE_2 0x4
#define FACE_3 0x8
#define FACE_4 0x10
#define FACE_5 0x20

static int face_codes[6]{ FACE_0, FACE_1, FACE_2, FACE_3, FACE_4, FACE_5};

void CompOp::Operation(float args[MAX_ARGS], GenerationContext *context)
{
	int dims = (fabs(context->currShape->geom_attrib.size.x - 0.0f) > EPSILON) +
		(fabs(context->currShape->geom_attrib.size.y - 0.0f) > EPSILON) +
		(fabs(context->currShape->geom_attrib.size.z - 0.0f) > EPSILON);

	int split_type = split_type_map[this->str_arg];
	GeomAttrib my_attrib = (inherit_attrib) ? context->currShape->geom_attrib : this->geom_attrib;

	if (dims = 3)
	{
		GeomAttrib new_attrib = GeomAttrib(my_attrib);

		new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (new_attrib.size.z * vec4(0, 0, 1, 1)));

		for (int i = 0; i < 4; ++i)
		{
			if (face_codes[i] & split_type)
			{
				split_ops[0]->inherit_attrib = false;
				split_ops[0]->geom_attrib = new_attrib;
				split_ops[0]->geom_attrib.size.z = 0.0f;
				if (i % 2 == 0)
					split_ops[0]->geom_attrib.size.x = my_attrib.size.x;
				else
					split_ops[0]->geom_attrib.size.x = my_attrib.size.z;

				split_ops[0]->op(context->curr_node->args, context);
			}

			mat4 rot;
			rot *= rotate((float)M_PI_2, vec3(0, 1, 0));

			new_attrib.x = (vec3)(rot * vec4(new_attrib.x, 1));
			new_attrib.y = (vec3)(rot * vec4(new_attrib.y, 1));
			new_attrib.z = (vec3)(rot * vec4(new_attrib.z, 1));

			if (i % 2 == 0)
				new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (new_attrib.size.x * vec4(0, 0, 1, 1)));
			else
				new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (new_attrib.size.z * vec4(0, 0, 1, 1)));
		}

		if (face_codes[4] & split_type)
		{
			GeomAttrib new_attrib = GeomAttrib(my_attrib);

			new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (new_attrib.size.y * vec4(0, 1, 0, 1)));
			new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.z) * (new_attrib.size.z * vec4(0, 0, 1, 1)));

			mat4 rot;
			rot *= rotate((float)M_PI_2, vec3(-1, 0, 0));

			new_attrib.x = (vec3)(rot * vec4(new_attrib.x, 1));
			new_attrib.y = (vec3)(rot * vec4(new_attrib.y, 1));
			new_attrib.z = (vec3)(rot * vec4(new_attrib.z, 1));

			split_ops[0]->inherit_attrib = false;
			split_ops[0]->geom_attrib = new_attrib;
			split_ops[0]->geom_attrib.size.z = 0.1f;

			split_ops[0]->op(context->curr_node->args, context);
		}

		if (face_codes[5] & split_type)
		{
			GeomAttrib new_attrib = GeomAttrib(my_attrib);

			//new_attrib.position += (vec3)(axis_to_mat(new_attrib.x, new_attrib.y, new_attrib.x) * (new_attrib.size.y * vec4(0, 0, 1, 1)));

			mat4 rot;
			//rot *= rotate((float)M_PI, vec3(0, 1, 0));
			rot *= rotate((float)M_PI_2, vec3(1, 0, 0));

			new_attrib.x = (vec3)(rot * vec4(new_attrib.x, 1));
			new_attrib.y = (vec3)(rot * vec4(new_attrib.y, 1));
			new_attrib.z = (vec3)(rot * vec4(new_attrib.z, 1));

			split_ops[0]->inherit_attrib = false;
			split_ops[0]->geom_attrib = new_attrib;
			split_ops[0]->geom_attrib.size.z = 0.1f;

			split_ops[0]->op(context->curr_node->args, context);
		}
	}
}

CompOp::CompOp()
{
	if (!initialized)
	{
		split_type_map["side faces"] = FACE_0 | FACE_1 | FACE_2 | FACE_3;
		initialized = true;
	}
}

ShapeOperation *CompOp::InstancePrototype()
{
	return new CompOp();
}