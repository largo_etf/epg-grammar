#pragma once

#include "Config.h"
#include <utility>

namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	class Symbol;
	class Expression;
	class ShapeOperation;

	//struct SymAndArgs
	//{
	//	Symbol* symbol;
	//	Expression* args[MAX_ARGS];
	//	void *extraArgs;

	//	void EvalArgs(float in[REAL_MAX_ARGS], float out[REAL_MAX_ARGS]);
	//};

	class Production
	{
	public:
		Production();

		int EvalCond(float in[REAL_MAX_ARGS]);

	private:
		static unsigned int idGen;
		unsigned int id;
		Symbol *left;

		Expression* cond;
		
		//SymAndArgs *rightSide[MAX_RIGHT_SIZE];
		ShapeOperation *rightSide[MAX_RIGHT_SIZE];
		float probabilty;

		friend class yy::parser;
		friend class Grammar;
	};
}