#pragma once

#define MAX_ARGS				5	// maximal number of arguments that could be passed to GrammarSymbol
#define MAX_EXPR_SIZE			15	// maximal number of tokens (operators and operands) in the expression, e.g. a+c/2 has 5 tokens
#define MAX_RIGHT_SIZE			50	// maximal number of GrammarSymbol on the right side of the production	
#define TRANSFORM_STACK_SIZE	5	// stack size of transforms when doing [ (push) and ] (pop)

namespace SplitGrammar
{
	#define DEFAULT_ATTRIB_CNT 3
	#define REAL_MAX_ARGS (MAX_ARGS + DEFAULT_ATTRIB_CNT)
}