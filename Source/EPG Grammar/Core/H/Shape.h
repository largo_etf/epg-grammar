#pragma once

#include <glm\mat4x4.hpp>
#include <glm\vec3.hpp>
#include "Config.h"
#include <list>

class Model;

namespace SplitGrammar
{
	class Symbol;
	struct GrammarModel;

	struct GeomAttrib
	{	
		glm::vec3 position;
		glm::vec3 x, y, z;
		glm::vec3 size;

		GeomAttrib();
	};

	class Shape
	{
	private:
		
		static bool boxLoaded;

		struct ModelAndTransform
		{
			Model *model;
			glm::mat4 transform;

			ModelAndTransform(Model *model, glm::mat4 transform);
		};

		GeomAttrib GeomAttribStack[TRANSFORM_STACK_SIZE];
		int sp;
	public:
		Shape();

		bool cached;

		Symbol *symbol;
		static Model* box;

		GeomAttrib original_attrib;
		GeomAttrib geom_attrib;
		GeomAttrib shape_origin_attrib;

		std::list<ModelAndTransform> models;
		void AddModel(Model *model);

		glm::mat4 GetTransform();

		static glm::mat4 GetTransform(GeomAttrib &attrib);
		GeomAttrib GetTotalAttrib(GeomAttrib &world);

		void Push();
		void Pop();
	
		void Draw(glm::mat4 world);
		void DrawScope();
	};
}