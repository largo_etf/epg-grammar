#pragma once

#include "Config.h"
#include <map>
#include <string>
#include "Shape.h"
#include <glm\mat4x4.hpp>

namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	class GenerationContext;
	//class Symbol;
	class Expression;

	class ShapeOperation
	{
	public:
		ShapeOperation();
		std::string name;

		bool inherit_attrib;
		GeomAttrib geom_attrib;
		glm::mat4 transform;

		void op(float parent_args[MAX_ARGS], GenerationContext *context);

	protected:
		ShapeOperation *split_ops[MAX_ARGS];
		int subsplits_cnt;
		Expression *args[MAX_ARGS];
		int rel_map[MAX_ARGS];
		char *str_arg;

		virtual ShapeOperation *InstancePrototype() = 0;

		
		virtual void Operation(float args[MAX_ARGS], GenerationContext *context) = 0;
		void EvalArgs(float in[REAL_MAX_ARGS], float out[MAX_ARGS]);

		friend class Grammar;
		friend class yy::parser;
		friend class OperationFactory;
	};

}