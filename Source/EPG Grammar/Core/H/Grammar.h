#pragma once
#pragma warning(disable : 4005)

#include <map>
#include <list>
#include <glm\mat4x4.hpp>
#include "Config.h"

class Model;
class Material;
class GeometryBatch;

namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	class Production;
	class Symbol;
	class Shape;
	struct GrammarModel;

	class GrammarNode
	{
	private:
		GrammarNode *parent;
		//GrammarNode *children[MAX_RIGHT_SIZE];
		std::list<GrammarNode*> children;
		int childernCnt;
		Shape *shape;

	public:
		GrammarNode(Shape* shape, GrammarNode *parent = 0);
		
		friend class GrammarTree;
		friend class Grammar;
	};

	class GrammarTree
	{
	private:
		GrammarNode *root;
		GrammarNode *curr;
	public:
		GrammarTree(GrammarNode *root = 0);

		GrammarNode* AddChild(Shape *shape);

		void Draw(glm::mat4 world);
		void DrawScopes();

		friend class Grammar;
	};

	class GenerationContext
	{
	public:
		struct QueueNode
		{
			GrammarNode *node;
			Production *pr;
			float args[MAX_ARGS];
			char *str_arg;
		};
		GenerationContext();

		Grammar *grammar;
		Shape *currShape;
		GrammarModel* grammarModel;
		std::list<QueueNode*> queue;
		QueueNode *curr_node;

		friend class Grammar;
	};

	struct GrammarModel
	{
		GrammarModel();
		~GrammarModel();

		GrammarTree *tree;
		
		GeometryBatch* batch;
		std::map<std::string, Material*> materials;
		Material* GetMaterial(std::string);
		void SaveMaterial(Material* mat, std::string);

		void Draw(glm::mat4 world);
		void DrawScopes();
	};

	class Grammar
	{
	public:
		Grammar();

		void AddProduction(Production *productionRule);
		Production *chooseProduction(Symbol *symbol, float in_args[MAX_ARGS], GenerationContext *context);

		GrammarModel *Generate(float args[MAX_ARGS]);
	private:
		std::map<std::string, std::list<Production*>>  productions;
		std::map<std::string, Symbol*> symbols;

		Production* startProduction;

		void addBasicSymbols();

		void cpuGenerate(GenerationContext *context);
		

		friend class yy::parser;
	};
}