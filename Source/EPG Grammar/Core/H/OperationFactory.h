#pragma once

#include <map>
#include <string>

namespace SplitGrammar
{
	class ShapeOperation;

	class OperationFactory
	{
	private:
		static std::map<std::string, ShapeOperation*> operations;

		static bool initialized;
	public:
		static ShapeOperation *GetOperation(std::string);
		static int RegisterOperation(ShapeOperation *op, std::string op_ident);
	};
}