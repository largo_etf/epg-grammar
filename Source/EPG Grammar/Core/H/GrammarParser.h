#pragma once

#include <iostream>

extern FILE* yyin;

namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	class Grammar;

	class GrammarParser
	{
	public:
		GrammarParser();
		Grammar* Parse(char *grammarSource);

	private:
		Grammar* grammar;

		friend class yy::parser;
	};
}