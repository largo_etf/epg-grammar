#pragma once

#include <string>
#include "Config.h"


namespace yy
{
	class parser;
}

namespace SplitGrammar
{
	typedef enum { UNKNOWN, TERMINAL, NONTERMINAL } SymbolType;
	typedef enum { VOID = 0x0, FP_VAR = 0x1, FP_CONST = 0x2, ADD = 0x4, SUB = 0x8, MUL = 0x10, DIV = 0x20, MOD = 0x40, GT = 0x80, GTE = 0x100, LT = 0x200, LTE = 0x400, EQ = 0x800, NEQ = 0x1000, LAND = 0X2000, LOR = 0x4000, INT_VAR = 0x8000, INT_CONST = 0x10000, ASSIGN = 0x20000 } Code;

	class ShapeOperation;
	struct SymAndArgs;

	union ArgValue
	{
		ArgValue(int i = 0) : intVal(i) {}
		ArgValue(float f) : floatVal(f) {}
		int intVal;
		float floatVal;
	};

	struct ExprToken
	{
		ExprToken(Code code = VOID, ArgValue val = ArgValue(0));
		Code code;
		ArgValue val;
	};

	class Symbol
	{
	public:
		Symbol(std::string value, SymbolType type = UNKNOWN);

		SymbolType Type();
		std::string Value();
	private:
		std::string value;
		ShapeOperation *op;
		SymbolType type;

		friend class yy::parser;
		friend class Grammar;
	};


	class Stack
	{
	public:
		Stack();

		void Push(ArgValue elem);
		ArgValue Pop();

		bool Empty();
	private:
		ArgValue stack[MAX_EXPR_SIZE];
		int i; // free location
	};

	class Expression
	{
	public:
		ArgValue evalExpr(float in[REAL_MAX_ARGS]);
	private:
		ExprToken expr[MAX_EXPR_SIZE];
		
		void calc(Code op, Stack *stack);

		friend class yy::parser;
		friend struct SymAndArgs;
		friend class Grammar;
		friend class Production;
		
	};


}