#include "Symbol.h"

using namespace SplitGrammar;

Symbol::Symbol(std::string value, SymbolType type) : value(value), type(type), op(0) {}

SymbolType Symbol::Type() { return type; }

std::string Symbol::Value() { return value; }


// **** Stack ****

Stack::Stack() : i(0) {}
ArgValue Stack::Pop() { return stack[--i]; }
void Stack::Push(ArgValue elem) { stack[i++] = elem; }
bool Stack::Empty() { return i == 0; }

ExprToken::ExprToken(Code code, ArgValue val)
	: code(code), val(val)
{
}

// **** Expression

ArgValue Expression::evalExpr(float in[REAL_MAX_ARGS])
{
	Stack s;
	ArgValue elem;

	for (int i = 0; i < MAX_EXPR_SIZE; ++i)
	{
		if (expr[i].code & (FP_VAR | FP_CONST))
		{
			elem.floatVal = in[expr[i].val.intVal * (expr[i].val.intVal < REAL_MAX_ARGS)] * (expr[i].code == FP_VAR) + expr[i].val.floatVal * (expr[i].code == FP_CONST);
			s.Push(elem);
		}
		else if (expr[i].code != VOID)
			calc(expr[i].code, &s);
	}

	if (s.Empty())
		return FP_INFINITE;
	else
		return s.Pop();
}

void Expression::calc(Code op, Stack *s)
{
	ArgValue val1 = s->Pop();
	ArgValue val2 = s->Pop();
	float val1_float = val1.floatVal;
	float val2_float = val2.floatVal;
	float val1_div;

	ArgValue res;
	res.intVal = 0;
	// Provera deljenja s nulom. U slucaju da se deli sa nulom
	// Vestacki se val2 povecava, ali samo u slucaju da opracija nije deljenje.
	// Ako operacija jeste deljenje i val2 jeste 0.0 onda je to korisnikova greska.
	val1_div = val1_float + 1.0f * (val1_float == 0.0 && op != DIV);
	res.floatVal += (val2_float + val1_float) * (op == ADD) +
		(val2_float - val1_float) * (op == SUB) +
		(val2_float * val1_float) * (op == MUL) +
		(val2_float / val1_div) * (op == DIV);

	int val1_int = val1.intVal;
	int val2_int = val2.intVal;

	res.intVal += (val2_int > val1_int) * (op == GT) +
		(val2_int < val1_int) * (op == LT) +
		(val2_int >= val1_int) * (op == GTE) +
		(val2_int <= val1_int) * (op == LTE) +
		(val2_int == val1_int) * (op == EQ) +
		(val2_int != val1_int) * (op == NEQ);
	
	s->Push(res);
}