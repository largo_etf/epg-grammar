#include "GrammarParser.h"
#include "Grammar.h"
#include "parser.hpp"

using namespace SplitGrammar;

GrammarParser::GrammarParser() : grammar(0) 
{
}

Grammar* GrammarParser::Parse(char *grammarSource)
{
	yyin = fopen(grammarSource, "r");
	yy::parser parser(this);
	
	parser.parse();

	return grammar;
}