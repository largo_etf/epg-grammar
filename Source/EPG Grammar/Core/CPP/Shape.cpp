#include "Shape.h"
#include "Symbol.h"
#include "Model.h"
#include "DSZMathHeader.h"
#include "DSZMaterial.h"
#include "Globals.h"
#include "DSZMath.h"

using namespace SplitGrammar;
using namespace glm;

Model* Shape::box = new Model();
bool Shape::boxLoaded = false;

Shape::Shape()
	:sp(0), cached(false)
{
	if (!boxLoaded)
	{
		Mesh *box_mesh = new Mesh();
		box_mesh->GenerateCube();
		box->AddMesh(box_mesh);

		DSZMaterial *mat = new DSZMaterial(&SimpleShader);
		mat->color = vec4(0.5f, 0.5f, 0.5f, 0.5f);
		mat->UseLighting = false;
		mat->UseStaticColor();

		box->SetAllMeshesMaterial(mat);

		boxLoaded = true;
	}
}

void Shape::Draw(mat4 world)
{
	for (std::list<ModelAndTransform>::iterator it = models.begin(); it != models.end(); it++)
	{
		(*it).model->Draw(world * (*it).transform);
	}
}

void Shape::DrawScope()
{
	box->Draw(GetTransform(original_attrib));
}

void Shape::Push()
{
	GeomAttribStack[sp++] = geom_attrib;
}

void Shape::Pop()
{
	geom_attrib = GeomAttribStack[--sp];
}

GeomAttrib::GeomAttrib()
	: position(0, 0, 0), x(1, 0, 0), y(0, 1, 0), z(0, 0, 1), size(1, 1, 1)
{

}


Shape::ModelAndTransform::ModelAndTransform(Model *model, glm::mat4 transform)
	: model(model), transform(transform)
{
}

glm::mat4 Shape::GetTransform(GeomAttrib &attrib)
{
	mat4 mat;
	mat *= translate(attrib.position);
	mat *= axis_to_mat(attrib.x, attrib.y, attrib.z);
	mat *= scale(attrib.size);

	return mat;
}

glm::mat4 Shape::GetTransform()
{
	GeomAttrib origin_normalized = shape_origin_attrib;
	origin_normalized.position.x *= geom_attrib.size.x / shape_origin_attrib.size.x;
	origin_normalized.position.y *= geom_attrib.size.y / shape_origin_attrib.size.y;
	origin_normalized.position.z *= geom_attrib.size.z / shape_origin_attrib.size.z;

	mat4 world = GetTransform(geom_attrib);
	//world *= scale(1.0f / geom_attrib.size);

	mat4 local = GetTransform(shape_origin_attrib);
	//local *= scale(1.0f / origin_normalized.size);

	mat4 total = world * local;
	//total *= translate(-shape_origin_attrib.position);
	//total *= scale(geom_attrib.size / shape_origin_attrib.size);

	return total;
}

GeomAttrib Shape::GetTotalAttrib(GeomAttrib &world_attrib)
{
	GeomAttrib attr;
	attr.size = world_attrib.size / shape_origin_attrib.size;

	mat4 world = axis_to_mat(world_attrib.x, world_attrib.y, world_attrib.z);
	mat4 local = axis_to_mat(shape_origin_attrib.x, shape_origin_attrib.y, shape_origin_attrib.z);
	mat4 total = world * local;

	vec4 new_pos = vec4(world_attrib.position, 1.0) + total * vec4(shape_origin_attrib.position * attr.size, 1.0f);
	attr.position = (vec3)new_pos;

	mat_to_axis(total, attr.x, attr.y, attr.z);
	
	return attr;
}

void Shape::AddModel(Model *model)
{
	models.push_back(ModelAndTransform(model, GetTransform()));

}
