#include "Grammar.h"
#include "Production.h"
#include "Symbol.h"
#include "Model.h"
#include "ShapeOperation.h"
#include "Shape.h"
#include <list>
#include "GeometryBatch.h"

using namespace std;
using namespace SplitGrammar;
using namespace glm;


//static float default_attrib[DEFAULT_ATTRIB_CNT] = { 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

Grammar::Grammar()
{
	addBasicSymbols();
}

void Grammar::AddProduction(Production *pr)
{
	list<Production*> &prList = productions[pr->left->Value()];
	list<Production*>::iterator it = prList.begin();
	
	for (; it != prList.end() && (*it)->probabilty < pr->probabilty; it++);
	prList.insert(it, pr);
}

GrammarModel *Grammar::Generate(float args[MAX_ARGS])
{
	GenerationContext *context = new GenerationContext();
	context->grammarModel = new GrammarModel();
	context->grammarModel->tree = new GrammarTree();
	
	//cpuGenerate(startProduction, real_args, 0, context);

	GenerationContext::QueueNode *new_info = new GenerationContext::QueueNode();
	//memcpy(new_info->args, default_attrib, DEFAULT_ATTRIB_CNT * sizeof(float));
	memcpy(new_info->args, args, MAX_ARGS * sizeof(float));
	new_info->pr = this->startProduction;
	Shape *shape = new Shape();
	shape->symbol = new_info->pr->left;
	//memcpy(&shape->geom_attrib, default_attrib, DEFAULT_ATTRIB_CNT * sizeof(float));

	new_info->node = context->grammarModel->tree->AddChild(shape);
	context->queue.push_back(new_info);

	context->grammar = this;

	cpuGenerate(context);


	GrammarModel *ret = context->grammarModel;
	delete context;
	return ret;
}


void Grammar::cpuGenerate(GenerationContext *context)
{
	while (context->queue.size() != 0)
	{
		GenerationContext::QueueNode *curr = context->queue.front();
		context->queue.pop_front();

		context->grammarModel->tree->curr = curr->node;
		context->currShape = curr->node->shape;
		context->curr_node = curr;

		for (int i = 0; i < MAX_RIGHT_SIZE && curr->pr->rightSide[i] != 0; ++i)
		{
			curr->pr->rightSide[i]->op(curr->args, context);
		}

	}
}

Production* Grammar::chooseProduction(Symbol *symbol, float in_args[REAL_MAX_ARGS], GenerationContext *context)
{
	if (productions.find(symbol->Value()) == productions.end())
		return NULL;

	float valid_prob_sum = 0.0f;

	list<Production*> &prList = productions[symbol->Value()];
	list<Production*> validPrList;
	list<Production*>::iterator it = prList.begin();

	for (; it != prList.end(); it++)
	{
		float args_ext[REAL_MAX_ARGS];
		memcpy(args_ext, &(context->currShape->geom_attrib.size), sizeof(float) * DEFAULT_ATTRIB_CNT);
		memcpy(args_ext + DEFAULT_ATTRIB_CNT, in_args, sizeof(float) * MAX_ARGS);
		if ((*it)->EvalCond(args_ext))
		{
			validPrList.push_back(*it);
			valid_prob_sum += (*it)->probabilty;
		}
	}

	double x = ((double)rand() / (double)RAND_MAX);
	double s = 0.0;

	it = validPrList.begin();
	float k = 1.0f / valid_prob_sum;
	for (; it != validPrList.end(); it++)
	{
		s += (*it)->probabilty * k;
		if (x <= s)
			break;
	}

	return it != validPrList.end() ? *it : NULL;
}

void Grammar::addBasicSymbols()
{
	//symbols["I"] = new Symbol("I", TERMINAL);
	//symbols["T"] = new Symbol("T", TERMINAL);
	//symbols["R"] = new Symbol("R", TERMINAL);
	//symbols["S"] = new Symbol("S", TERMINAL);
	//symbols["["] = new Symbol("[", TERMINAL);
	//symbols["]"] = new Symbol("]", TERMINAL);
}


GrammarModel::GrammarModel()
{
	batch = new GeometryBatch();
}

GrammarModel::~GrammarModel()
{
	delete batch;
}

void GrammarModel::Draw(mat4 world)
{
	//tree->Draw(world);
	/*for (list<Shape*>::iterator it = draw_cache.begin(); it != draw_cache.end(); it++)
	{
		(*it)->Draw(world);
	}*/
	batch->Draw(world, this);
}

void GrammarModel::DrawScopes()
{
	tree->DrawScopes();
}

Material* GrammarModel::GetMaterial(std::string mat_name)
{
	if (materials.find(mat_name) != materials.end())
		return materials[mat_name];
	else
		return 0;
}

void GrammarModel::SaveMaterial(Material *mat, std::string mat_name)
{
	materials[mat_name] = mat;
}



GenerationContext::GenerationContext() { }


GrammarNode::GrammarNode(Shape *shape, GrammarNode *parent) : shape(shape), parent(parent), childernCnt(0)
{
	//memset(children, 0, sizeof(GrammarNode*) * MAX_RIGHT_SIZE);
}

GrammarTree::GrammarTree(GrammarNode *root) : root(root), curr(root) {}

GrammarNode* GrammarTree::AddChild(Shape *shape)
{
	GrammarNode *newNode = new GrammarNode(shape, curr);

	if (root == 0)
	{
		root = newNode;
		curr = root;
	}
	else
	{
		//curr->children[curr->childernCnt++] = newNode;
		curr->children.push_back(newNode);
		curr->childernCnt++;
	}

	return newNode;
}

void GrammarTree::Draw(mat4 world)
{
	list<GrammarNode*> queue;
	queue.push_back(root);

	while (queue.size() != 0)
	{
		GrammarNode *node = queue.front();
		queue.pop_front();

		for (list<GrammarNode*>::iterator i = node->children.begin(); i != node->children.end(); ++i)
			queue.push_back(*i);

		node->shape->Draw(world);
	}
}

void GrammarTree::DrawScopes()
{
	list<GrammarNode*> queue;
	queue.push_back(root);

	while (queue.size() != 0)
	{
		GrammarNode *node = queue.front();
		queue.pop_front();

		for (list<GrammarNode*>::iterator i = node->children.begin(); i != node->children.end(); ++i)
			queue.push_back(*i);

		node->shape->DrawScope();
	}
}

