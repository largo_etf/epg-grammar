#include "OperationFactory.h"
#include "ShapeOperation.h"
#include "InstanceOperation.h"
#include "BasicOperations.h"
#include "BasicSplit.h"
#include "Subdiv.h"
#include "Repeat.h"
#include "CompOp.h"
#include "PrintOp.h"
#include "TexOp.h"
#include <iostream>

using namespace SplitGrammar;
using namespace std;

std::map<std::string, ShapeOperation*> OperationFactory::operations;
bool OperationFactory::initialized = false;

ShapeOperation *OperationFactory::GetOperation(string op)
{
	if (!initialized)
	{
		operations["I"] = new InstanceOperation();

		operations["T"] = new TranslateOperation(ASSIGN, false);
		operations["T+"] = new TranslateOperation(ADD, false);
		operations["T-"] = new TranslateOperation(SUB, false);
		operations["Tl"] = new TranslateOperation(ASSIGN, true);
		operations["Tl+"] = new TranslateOperation(ADD, true);
		operations["Tl-"] = new TranslateOperation(SUB, true);

		operations["R"] = new RotateOperation(ASSIGN, false);
		operations["R+"] = new RotateOperation(ADD, false);
		operations["R-"] = new RotateOperation(SUB, false);
		operations["Rl"] = new RotateOperation(ASSIGN, true);
		operations["Rl+"] = new RotateOperation(ADD, true);
		operations["Rl-"] = new RotateOperation(SUB, true);

		operations["S"] = new ScaleOperation(ASSIGN, false);
		operations["S+"] = new ScaleOperation(ADD, false);
		operations["S-"] = new ScaleOperation(SUB, false);
		operations["S*"] = new ScaleOperation(MUL, false);
		operations["S/"] = new ScaleOperation(DIV, false);
		operations["Sl"] = new ScaleOperation(ASSIGN, true);
		operations["Sl+"] = new ScaleOperation(ADD, true);
		operations["Sl-"] = new ScaleOperation(SUB, true);
		operations["Sl*"] = new ScaleOperation(MUL, true);
		operations["Sl/"] = new ScaleOperation(DIV, true);

		operations["["] = new PushOperation();
		operations["]"] = new PopOperation();
		operations["BasicSplit"] = new BasicSplit();
		operations["D"] = new DebugScope();
		operations["eps"] = new Epsilon();
		operations["Subdiv"] = new Subdiv();
		operations["Repeat"] = new RepeatOp();
		operations["Comp"] = new CompOp();

		operations["Tex"] = new TexOp();
		operations["Col"] = new ColOp();
		operations["Mat"] = new MatOp();

		operations["Print"] = new PrintOp();

		initialized = true;
	}

	if (operations.find(op) == operations.end())
		return 0;
	else
		return operations[op]->InstancePrototype();
}

int OperationFactory::RegisterOperation(ShapeOperation *op, string name)
{
	if (operations.find(name) != operations.end())
	{
		cout << "Operation identifier conflict : " << name << "! Operation not registered!\n";
		return 0;
	}
	operations[name] = op;
	return 1;
}