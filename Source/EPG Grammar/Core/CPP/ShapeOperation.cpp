#include "ShapeOperation.h"
#include "Symbol.h"
#include "Grammar.h"
#include "Shape.h"
using namespace SplitGrammar;

ShapeOperation::ShapeOperation()
	:subsplits_cnt(0), geom_attrib(), transform(), inherit_attrib(true)
{
	memset(split_ops, 0, sizeof(ShapeOperation*) * MAX_ARGS);
	memset(args, 0, sizeof(ShapeOperation*) * MAX_ARGS);
	str_arg = 0;
}

void ShapeOperation::op(float parent_args[MAX_ARGS], GenerationContext *context)
{
	float parent_args_ext[REAL_MAX_ARGS];
	float my_args[MAX_ARGS];
	memcpy(parent_args_ext, &(context->currShape->geom_attrib.size), sizeof(float) * 3);
	memcpy(parent_args_ext + DEFAULT_ATTRIB_CNT, parent_args, sizeof(float) * MAX_ARGS);

	EvalArgs(parent_args_ext, my_args);

	Operation(my_args, context);
}

void ShapeOperation::EvalArgs(float in[REAL_MAX_ARGS], float out[MAX_ARGS])
{
	for (int i = 0; i < MAX_ARGS && args[i] != NULL; ++i)
		out[i] = args[i]->evalExpr(in).floatVal;
}