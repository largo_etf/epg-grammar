#include "Production.h"
#include "Symbol.h"
#include <cstring>

using namespace SplitGrammar;

unsigned int Production::idGen = 0;

Production::Production()
	: id(idGen++), cond(0)
{
	memset(rightSide, 0, sizeof(ShapeOperation*)* MAX_RIGHT_SIZE);
}



int Production::EvalCond(float in[REAL_MAX_ARGS])
{

	return cond != 0 ? cond->evalExpr(in).intVal : 1;
}
