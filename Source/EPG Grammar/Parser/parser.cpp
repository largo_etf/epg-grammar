// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.



# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "parser.hpp"

// User implementation prologue.


// Unqualified %code blocks.


	
	#include "Grammar.h"
	#include "Production.h"
	#include "OperationFactory.h"
	#include "BasicSplit.h"
	#include <map>
	#include "Config.h"

	using namespace SplitGrammar;
	using namespace std;

	static Grammar *grammar;

	static SymbolType currType = UNKNOWN;

	static Production *currRule = 0;
	static map<string, int> formals;
	static int formalsCnt = DEFAULT_ATTRIB_CNT;

	//static SymAndArgs *currSymbol = 0;
	static list<ShapeOperation*> shape_op_stack;
	static list<int> remove_flag_stack;
	static int stack_level = 0;
	static Expression *currExpr = 0;
	static int symbolsCnt = 0;
	static int argCnt = 0;
	static int exprTokenCnt = 0;
	//static bool is_basic = false;

	static bool startSet = false;

	yy::parser::symbol_type yylex(GrammarParser *grammarParser);




#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif



// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {


  /// Build a parser object.
  parser::parser (SplitGrammar::GrammarParser *grammarParser_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      grammarParser (grammarParser_yyarg)
  {}

  parser::~parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  parser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  parser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  parser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  parser::symbol_number_type
  parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  parser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s)
  {
      switch (that.type_get ())
    {
      case 26: // LOR_OP
      case 27: // LAND_OP
      case 67: // Addop
      case 68: // Mulop
      case 69: // Relop
        value.move< SplitGrammar::Code > (that.value);
        break;

      case 29: // STR_CONST
        value.move< char* > (that.value);
        break;

      case 28: // FP_CONST
        value.move< float > (that.value);
        break;

      case 31: // Grammar
      case 32: // SymbolList
      case 33: // SymbolDeclLine
      case 34: // SymType
      case 35: // DeclList
      case 36: // Decl
      case 37: // RuleList
      case 38: // Rule
      case 39: // Left
      case 40: // LeftName
      case 41: // FormalParamsOpt
      case 42: // FormalParamList
      case 43: // FormalParam
      case 45: // ShapeOp
      case 46: // PiecesOpt
      case 47: // Pieces
      case 48: // Piece
      case 50: // ArgsOpt
      case 51: // ArgsList
      case 52: // Arg
      case 54: // RelOpt
      case 55: // Expr
      case 56: // TermList
      case 57: // Term
      case 58: // Factor
      case 59: // CondExprOpt
      case 61: // CondExpr
      case 62: // CondTermList
      case 63: // CondTerm
      case 64: // CondFactorList
      case 65: // CondFactor
      case 66: // ExprList
        value.move< int > (that.value);
        break;

      case 3: // IDENT
      case 4: // PRODUCE
      case 5: // SEMI
      case 6: // COLON
      case 7: // COMMA
      case 8: // LPAREN
      case 9: // RPAREN
      case 10: // LBRACE
      case 11: // RBRACE
      case 12: // TERM
      case 13: // NON_TERM
      case 14: // ADD_OP
      case 15: // SUB_OP
      case 16: // MUL_OP
      case 17: // DIV_OP
      case 18: // MOD_OP
      case 19: // GT_OP
      case 20: // GTE_OP
      case 21: // LT_OP
      case 22: // LTE_OP
      case 23: // EQ_OP
      case 24: // NEQ_OP
      case 25: // REL
      case 49: // Ident
      case 70: // ArittmeticOp
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 26: // LOR_OP
      case 27: // LAND_OP
      case 67: // Addop
      case 68: // Mulop
      case 69: // Relop
        value.copy< SplitGrammar::Code > (that.value);
        break;

      case 29: // STR_CONST
        value.copy< char* > (that.value);
        break;

      case 28: // FP_CONST
        value.copy< float > (that.value);
        break;

      case 31: // Grammar
      case 32: // SymbolList
      case 33: // SymbolDeclLine
      case 34: // SymType
      case 35: // DeclList
      case 36: // Decl
      case 37: // RuleList
      case 38: // Rule
      case 39: // Left
      case 40: // LeftName
      case 41: // FormalParamsOpt
      case 42: // FormalParamList
      case 43: // FormalParam
      case 45: // ShapeOp
      case 46: // PiecesOpt
      case 47: // Pieces
      case 48: // Piece
      case 50: // ArgsOpt
      case 51: // ArgsList
      case 52: // Arg
      case 54: // RelOpt
      case 55: // Expr
      case 56: // TermList
      case 57: // Term
      case 58: // Factor
      case 59: // CondExprOpt
      case 61: // CondExpr
      case 62: // CondTermList
      case 63: // CondTerm
      case 64: // CondFactorList
      case 65: // CondFactor
      case 66: // ExprList
        value.copy< int > (that.value);
        break;

      case 3: // IDENT
      case 4: // PRODUCE
      case 5: // SEMI
      case 6: // COLON
      case 7: // COMMA
      case 8: // LPAREN
      case 9: // RPAREN
      case 10: // LBRACE
      case 11: // RBRACE
      case 12: // TERM
      case 13: // NON_TERM
      case 14: // ADD_OP
      case 15: // SUB_OP
      case 16: // MUL_OP
      case 17: // DIV_OP
      case 18: // MOD_OP
      case 19: // GT_OP
      case 20: // GTE_OP
      case 21: // LT_OP
      case 22: // LTE_OP
      case 23: // EQ_OP
      case 24: // NEQ_OP
      case 25: // REL
      case 49: // Ident
      case 70: // ArittmeticOp
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    return *this;
  }


  template <typename Base>
  inline
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " (";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  parser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    // User initialization code.
    
{
	grammar = new Grammar();
}



    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (grammarParser));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 26: // LOR_OP
      case 27: // LAND_OP
      case 67: // Addop
      case 68: // Mulop
      case 69: // Relop
        yylhs.value.build< SplitGrammar::Code > ();
        break;

      case 29: // STR_CONST
        yylhs.value.build< char* > ();
        break;

      case 28: // FP_CONST
        yylhs.value.build< float > ();
        break;

      case 31: // Grammar
      case 32: // SymbolList
      case 33: // SymbolDeclLine
      case 34: // SymType
      case 35: // DeclList
      case 36: // Decl
      case 37: // RuleList
      case 38: // Rule
      case 39: // Left
      case 40: // LeftName
      case 41: // FormalParamsOpt
      case 42: // FormalParamList
      case 43: // FormalParam
      case 45: // ShapeOp
      case 46: // PiecesOpt
      case 47: // Pieces
      case 48: // Piece
      case 50: // ArgsOpt
      case 51: // ArgsList
      case 52: // Arg
      case 54: // RelOpt
      case 55: // Expr
      case 56: // TermList
      case 57: // Term
      case 58: // Factor
      case 59: // CondExprOpt
      case 61: // CondExpr
      case 62: // CondTermList
      case 63: // CondTerm
      case 64: // CondFactorList
      case 65: // CondFactor
      case 66: // ExprList
        yylhs.value.build< int > ();
        break;

      case 3: // IDENT
      case 4: // PRODUCE
      case 5: // SEMI
      case 6: // COLON
      case 7: // COMMA
      case 8: // LPAREN
      case 9: // RPAREN
      case 10: // LBRACE
      case 11: // RBRACE
      case 12: // TERM
      case 13: // NON_TERM
      case 14: // ADD_OP
      case 15: // SUB_OP
      case 16: // MUL_OP
      case 17: // DIV_OP
      case 18: // MOD_OP
      case 19: // GT_OP
      case 20: // GTE_OP
      case 21: // LT_OP
      case 22: // LTE_OP
      case 23: // EQ_OP
      case 24: // NEQ_OP
      case 25: // REL
      case 49: // Ident
      case 70: // ArittmeticOp
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }



      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:

    {
	grammarParser->grammar = grammar;
}

    break;

  case 6:

    { currType = NONTERMINAL; }

    break;

  case 7:

    { currType = TERMINAL; }

    break;

  case 8:

    { yylhs.value.as< int > () = 0; }

    break;

  case 9:

    { yylhs.value.as< int > () = 0; }

    break;

  case 10:

    { grammar->symbols[yystack_[0].value.as< std::string > ()] = new SplitGrammar::Symbol(yystack_[0].value.as< std::string > (), currType); yylhs.value.as< int > () = 0; }

    break;

  case 11:

    {

}

    break;

  case 12:

    {

}

    break;

  case 13:

    {
	currRule->probabilty = yystack_[1].value.as< float > ();
	grammar->AddProduction(currRule);

	if (!startSet)
	{
		grammar->startProduction = currRule;
		startSet = true;
	}

	currRule = 0;
}

    break;

  case 14:

    { yylhs.value.as< int > () = 0; }

    break;

  case 15:

    {
	currRule = new Production();
	currRule->left = grammar->symbols[yystack_[0].value.as< std::string > ()];
	//formals.clear();
	clearFormals();
	formalsCnt = DEFAULT_ATTRIB_CNT;
	symbolsCnt = 0;
	yylhs.value.as< int > () = 0;
}

    break;

  case 16:

    { yylhs.value.as< int > () = 0; }

    break;

  case 17:

    { yylhs.value.as< int > () = 0; }

    break;

  case 18:

    { yylhs.value.as< int > () = 0; }

    break;

  case 19:

    { yylhs.value.as< int > () = 0; }

    break;

  case 20:

    {
	formals[yystack_[0].value.as< std::string > ()] = formalsCnt++;
	yylhs.value.as< int > () = 0;
}

    break;

  case 21:

    {

}

    break;

  case 22:

    {

}

    break;

  case 23:

    {
	ShapeOperation *op = shape_op_stack.back();
	shape_op_stack.pop_back();

	if (shape_op_stack.size() == 0)
	{
		currRule->rightSide[symbolsCnt++] = op;
	}
	// stack_level--;
	// if (stack_level == 0)
	// {
	// 	while(shape_op_stack.size() != 0)
	// 	{
	// 		currRule->rightSide[symbolsCnt++] = shape_op_stack.front();
	// 		shape_op_stack.pop_front();
	// 	}
	// }
}

    break;

  case 24:

    { yylhs.value.as< int > () = 0; }

    break;

  case 25:

    { yylhs.value.as< int > () = 0; }

    break;

  case 28:

    { yylhs.value.as< int > () = 0; }

    break;

  case 29:

    {
	argCnt = 0;
	string opname = yystack_[1].value.as< std::string > () + yystack_[0].value.as< std::string > ();
	if (grammar->symbols.find(opname) != grammar->symbols.end())
	{
		//is_basic = true;
		BasicSplit *basicSplit = (BasicSplit*)OperationFactory::GetOperation("BasicSplit");
		basicSplit->name = opname;
		basicSplit->symbol = grammar->symbols[opname];
		if (!shape_op_stack.empty())
		{
			shape_op_stack.back()->split_ops[shape_op_stack.back()->subsplits_cnt++] = basicSplit;
		}
		shape_op_stack.push_back(basicSplit);
		stack_level++;
	}
	else
	{
		//is_basic = false;
		ShapeOperation *op = OperationFactory::GetOperation(opname);
		if (op == 0)
			printf("No operation or symbol!\n");
		else
		{
			op->name = opname;
			if (!shape_op_stack.empty())
			{
				shape_op_stack.back()->split_ops[shape_op_stack.back()->subsplits_cnt++] = op;
			}
			shape_op_stack.push_back(op);
			stack_level++;
		}
	}
}

    break;

  case 30:

    { yylhs.value.as< int > () = 0; }

    break;

  case 31:

    { yylhs.value.as< int > () = 0; }

    break;

  case 32:

    { yylhs.value.as< int > () = 0; }

    break;

  case 33:

    { yylhs.value.as< int > () = 0; }

    break;

  case 34:

    {
	currExpr = new Expression();
	shape_op_stack.back()->args[argCnt] = currExpr;
	shape_op_stack.back()->rel_map[argCnt] = 0;
	//currSymbol->args[argCnt] = currExpr;
	exprTokenCnt = 0;
}

    break;

  case 35:

    {
	argCnt++;
	yylhs.value.as< int > () = 0;
}

    break;

  case 36:

    {
	shape_op_stack.back()->rel_map[argCnt] = 1;
}

    break;

  case 37:

    { yylhs.value.as< int > () = 0; }

    break;

  case 38:

    { yylhs.value.as< int > () = 0; }

    break;

  case 39:

    {
	currExpr->expr[exprTokenCnt++] = ExprToken(yystack_[1].value.as< SplitGrammar::Code > ());
	yylhs.value.as< int > () = 0;
}

    break;

  case 40:

    { yylhs.value.as< int > () = 0; }

    break;

  case 41:

    {
	currExpr->expr[exprTokenCnt++] = ExprToken(yystack_[1].value.as< SplitGrammar::Code > ());
	yylhs.value.as< int > () = 0;
}

    break;

  case 42:

    { yylhs.value.as< int > () = 0 ; }

    break;

  case 43:

    {
	ArgValue token;
	token.floatVal = yystack_[0].value.as< float > ();
	currExpr->expr[exprTokenCnt++] = ExprToken(FP_CONST, token);
	yylhs.value.as< int > () = 0;
}

    break;

  case 44:

    {
	ArgValue token;
	token.intVal = formals[yystack_[0].value.as< std::string > ()];
	currExpr->expr[exprTokenCnt++] = ExprToken(FP_VAR, token);
	yylhs.value.as< int > () = 0;
}

    break;

  case 45:

    {
	char *str = new char[strlen(yystack_[0].value.as< char* > ()) + 1];
	strcpy(str, yystack_[0].value.as< char* > ());
	shape_op_stack.back()->str_arg = str;
	argCnt--;
}

    break;

  case 46:

    { yylhs.value.as< int > () = 0; }

    break;

  case 47:

    { 
	currExpr = new Expression();
	exprTokenCnt = 0;
	currRule->cond = currExpr;
}

    break;

  case 48:

    { yylhs.value.as< int > () = 0; }

    break;

  case 49:

    { yylhs.value.as< int > () = 0; }

    break;

  case 50:

    { yylhs.value.as< int > () = 0; }

    break;

  case 51:

    {
	currExpr->expr[exprTokenCnt++] = ExprToken(yystack_[1].value.as< SplitGrammar::Code > ());
	yylhs.value.as< int > () = 0;
}

    break;

  case 52:

    { yylhs.value.as< int > () = 0; }

    break;

  case 53:

    { yylhs.value.as< int > () = 0; }

    break;

  case 54:

    {
	currExpr->expr[exprTokenCnt++] = ExprToken(yystack_[1].value.as< SplitGrammar::Code > ());
	yylhs.value.as< int > () = 0;
}

    break;

  case 55:

    { yylhs.value.as< int > () = 0; }

    break;

  case 56:

    { yylhs.value.as< int > () = 0; }

    break;

  case 57:

    {
	currExpr->expr[exprTokenCnt++] = ExprToken(yystack_[1].value.as< SplitGrammar::Code > ());
	yylhs.value.as< int > () = 0;
}

    break;

  case 58:

    { yylhs.value.as< int > () = 0; }

    break;

  case 59:

    { yylhs.value.as< SplitGrammar::Code > () = ADD; }

    break;

  case 60:

    { yylhs.value.as< SplitGrammar::Code > () = SUB; }

    break;

  case 61:

    { yylhs.value.as< SplitGrammar::Code > () = MUL; }

    break;

  case 62:

    { yylhs.value.as< SplitGrammar::Code > () = DIV; }

    break;

  case 63:

    { yylhs.value.as< SplitGrammar::Code > () = MOD; }

    break;

  case 64:

    { yylhs.value.as< SplitGrammar::Code > () = GT; }

    break;

  case 65:

    { yylhs.value.as< SplitGrammar::Code > () = GTE; }

    break;

  case 66:

    { yylhs.value.as< SplitGrammar::Code > () = LT; }

    break;

  case 67:

    { yylhs.value.as< SplitGrammar::Code > () = LTE; }

    break;

  case 68:

    { yylhs.value.as< SplitGrammar::Code > () = EQ; }

    break;

  case 69:

    { yylhs.value.as< SplitGrammar::Code > () = NEQ; }

    break;

  case 70:

    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }

    break;

  case 71:

    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }

    break;

  case 72:

    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }

    break;

  case 73:

    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }

    break;

  case 74:

    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }

    break;

  case 75:

    { yylhs.value.as< std::string > () = ""; }

    break;



            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yysyntax_error_ (yystack_[0].state, yyla));
      }


    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }


      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.what());
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (state_type, const symbol_type&) const
  {
    return YY_("syntax error");
  }


  const signed char parser::yypact_ninf_ = -36;

  const signed char parser::yytable_ninf_ = -50;

  const signed char
  parser::yypact_[] =
  {
      -4,   -36,   -36,    36,    -1,   -36,    22,   -36,   -36,   -36,
      37,   -36,    38,    35,   -36,    26,   -36,   -36,    40,    39,
      44,   -36,   -36,    22,    45,    -2,   -36,    25,   -36,   -36,
       6,     1,   -36,    41,   -36,    -2,   -36,   -36,   -36,    24,
      12,   -36,   -36,    27,   -36,    23,   -36,    -5,    44,   -36,
     -36,   -36,   -36,   -36,   -36,   -36,    29,   -36,   -36,    42,
      46,   -36,   -36,    -2,   -36,   -36,   -36,    -2,    -2,    -2,
     -36,   -36,   -36,   -36,   -36,   -36,    -2,   -36,    49,    28,
     -36,    -2,    45,   -36,   -36,    12,   -36,   -36,   -36,   -36,
     -36,   -36,   -36,    31,   -36,     2,   -36,   -36,   -36,   -36,
     -36,   -36
  };

  const unsigned char
  parser::yydefact_[] =
  {
       0,     7,     6,     0,     0,     4,     0,     1,    15,     3,
       2,    12,    47,    17,    10,     0,     9,    11,     0,     0,
       0,    14,     5,     0,     0,     0,    20,     0,    19,     8,
      75,     0,    22,    31,    44,     0,    43,    45,    58,    38,
      40,    42,    48,    50,    52,    53,    55,    56,     0,    16,
      70,    71,    72,    73,    74,    29,     0,    21,    34,    25,
       0,    59,    60,     0,    61,    62,    63,     0,     0,     0,
      64,    65,    66,    67,    68,    69,     0,    18,     0,     0,
      33,     0,     0,    23,    46,    39,    41,    51,    54,    57,
      13,    34,    30,    37,    28,     0,    27,    32,    36,    35,
      24,    26
  };

  const signed char
  parser::yypgoto_[] =
  {
     -36,   -36,   -36,    47,   -36,   -36,    43,   -36,    48,   -36,
     -36,   -36,   -36,    11,   -36,   -21,   -36,   -36,   -34,   -36,
     -36,   -36,   -31,   -36,   -36,   -35,   -36,     0,    -3,   -36,
     -36,   -36,   -36,    -6,   -36,     3,   -36,   -36,   -36,   -36,
     -36
  };

  const signed char
  parser::yydefgoto_[] =
  {
      -1,     3,     4,     5,     6,    15,    16,    10,    11,    12,
      13,    21,    27,    28,    31,    94,    83,    95,    96,    33,
      59,    79,    80,    81,    99,    38,    39,    40,    41,    18,
      19,    42,    43,    44,    45,    46,    47,    63,    67,    76,
      55
  };

  const signed char
  parser::yytable_[] =
  {
      60,    34,     8,    32,    30,    30,    35,    56,     1,     2,
      57,     1,     2,   100,    70,    71,    72,    73,    74,    75,
      50,    51,    52,    53,    54,    14,    36,    37,    64,    65,
      66,    22,    48,    23,    49,    91,     7,    92,    61,    62,
       8,    89,   -49,    20,    24,    25,    93,    26,    30,    58,
      69,     9,    82,    68,    90,    84,    98,    78,    17,    77,
      97,   101,    87,    85,    86,     0,    29,     0,     0,     0,
       0,     0,    88
  };

  const signed char
  parser::yycheck_[] =
  {
      35,     3,     3,    24,     3,     3,     8,     6,    12,    13,
      31,    12,    13,    11,    19,    20,    21,    22,    23,    24,
      14,    15,    16,    17,    18,     3,    28,    29,    16,    17,
      18,     5,     7,     7,     9,     7,     0,     9,    14,    15,
       3,    76,     4,     8,     4,     6,    81,     3,     3,     8,
      27,     4,    10,    26,     5,     9,    25,    28,    10,    48,
      91,    95,    68,    63,    67,    -1,    23,    -1,    -1,    -1,
      -1,    -1,    69
  };

  const unsigned char
  parser::yystos_[] =
  {
       0,    12,    13,    31,    32,    33,    34,     0,     3,    33,
      37,    38,    39,    40,     3,    35,    36,    38,    59,    60,
       8,    41,     5,     7,     4,     6,     3,    42,    43,    36,
       3,    44,    45,    49,     3,     8,    28,    29,    55,    56,
      57,    58,    61,    62,    63,    64,    65,    66,     7,     9,
      14,    15,    16,    17,    18,    70,     6,    45,     8,    50,
      55,    14,    15,    67,    16,    17,    18,    68,    26,    27,
      19,    20,    21,    22,    23,    24,    69,    43,    28,    51,
      52,    53,    10,    46,     9,    57,    58,    63,    65,    55,
       5,     7,     9,    55,    45,    47,    48,    52,    25,    54,
      11,    48
  };

  const unsigned char
  parser::yyr1_[] =
  {
       0,    30,    31,    32,    32,    33,    34,    34,    35,    35,
      36,    37,    37,    38,    39,    40,    41,    41,    42,    42,
      43,    44,    44,    45,    46,    46,    47,    47,    48,    49,
      50,    50,    51,    51,    53,    52,    54,    54,    55,    56,
      56,    57,    57,    58,    58,    58,    58,    60,    59,    59,
      61,    62,    62,    63,    64,    64,    65,    66,    66,    67,
      67,    68,    68,    68,    69,    69,    69,    69,    69,    69,
      70,    70,    70,    70,    70,    70
  };

  const unsigned char
  parser::yyr2_[] =
  {
       0,     2,     2,     2,     1,     3,     1,     1,     3,     1,
       1,     2,     1,     7,     2,     1,     3,     0,     3,     1,
       1,     2,     1,     3,     3,     0,     2,     1,     1,     2,
       3,     0,     3,     1,     0,     3,     1,     0,     1,     3,
       1,     3,     1,     1,     1,     1,     3,     0,     3,     0,
       1,     3,     1,     1,     3,     1,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0
  };


#if YYDEBUG
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const parser::yytname_[] =
  {
  "EOF", "error", "$undefined", "IDENT", "PRODUCE", "SEMI", "COLON",
  "COMMA", "LPAREN", "RPAREN", "LBRACE", "RBRACE", "TERM", "NON_TERM",
  "ADD_OP", "SUB_OP", "MUL_OP", "DIV_OP", "MOD_OP", "GT_OP", "GTE_OP",
  "LT_OP", "LTE_OP", "EQ_OP", "NEQ_OP", "REL", "LOR_OP", "LAND_OP",
  "FP_CONST", "STR_CONST", "$accept", "Grammar", "SymbolList",
  "SymbolDeclLine", "SymType", "DeclList", "Decl", "RuleList", "Rule",
  "Left", "LeftName", "FormalParamsOpt", "FormalParamList", "FormalParam",
  "Right", "ShapeOp", "PiecesOpt", "Pieces", "Piece", "Ident", "ArgsOpt",
  "ArgsList", "Arg", "$@1", "RelOpt", "Expr", "TermList", "Term", "Factor",
  "CondExprOpt", "$@2", "CondExpr", "CondTermList", "CondTerm",
  "CondFactorList", "CondFactor", "ExprList", "Addop", "Mulop", "Relop",
  "ArittmeticOp", YY_NULLPTR
  };


  const unsigned short int
  parser::yyrline_[] =
  {
       0,   110,   110,   120,   121,   123,   125,   126,   128,   129,
     131,   138,   142,   147,   161,   163,   180,   181,   183,   184,
     186,   198,   203,   208,   228,   229,   231,   232,   234,   236,
     278,   279,   281,   282,   285,   285,   298,   303,   311,   313,
     319,   321,   327,   329,   336,   343,   350,   358,   358,   365,
     367,   369,   374,   376,   378,   383,   385,   387,   392,   400,
     401,   403,   404,   405,   407,   408,   409,   410,   411,   412,
     414,   415,   416,   417,   418,   419
  };

  // Print the state stack on the debug stream.
  void
  parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG



} // yy




void yy::parser::error(const std::string &m)
{
	std::cout << "Parser error : " << m << std::endl;
}

static void clearFormals()
{
	formals.clear();
	formals["Sx"] = 0;
	formals["Sy"] = 1;
	formals["Sz"] = 2;
}
