%{
	#include <stdio.h>
	#include "parser.hpp"
	#include "Grammar.h"
	#include "GrammarParser.h"
	#include "Symbol.h"
	#include <cstring>

	#define YY_NO_UNISTD_H
	#define YY_DECL yy::parser::symbol_type yylex(SplitGrammar::GrammarParser *grammarParser)
%}

%option noyywrap never-interactive


%{
  // Code run each time a pattern is matched.
  //# define YY_USER_ACTION  loc.columns (yyleng);
%}

%%

%{
  // Code run each time yylex is called.
  //loc.step ();
%}

" " 	//{ loc.step (); }
"\b" 	//{ loc.step (); }
"\t" 	//{ loc.step (); }
"\r\n" 	//{ loc.step (); }
"\f" 	//{ loc.step (); }
"\n" 	//{ loc.step (); }


"->"							{ printf("lex PRODUCE %s\n", yytext); return yy::parser::make_PRODUCE(yytext); }
"r"								{ printf("lex REL %s\n", yytext); return yy::parser::make_REL(yytext); }
";"								{ printf("lex SEMI %s\n", yytext); return yy::parser::make_SEMI(yytext); }
":"								{ printf("lex COLON %s\n", yytext); return yy::parser::make_COLON(yytext); }
"("								{ printf("lex LPAREN %s\n", yytext); return yy::parser::make_LPAREN(yytext); }
")"								{ printf("lex RPAREN %s\n", yytext); return yy::parser::make_RPAREN(yytext); }
"{"								{ printf("lex LBRACE %s\n", yytext); return yy::parser::make_LBRACE(yytext); }
"}"								{ printf("lex RBRACE %s\n", yytext); return yy::parser::make_RBRACE(yytext); }
"+"								{ printf("lex PLUS %s\n", yytext); return yy::parser::make_ADD_OP(yytext); }
"-"								{ printf("lex MINUS %s\n", yytext); return yy::parser::make_SUB_OP(yytext); }
"/"								{ printf("lex DIV %s\n", yytext); return yy::parser::make_DIV_OP(yytext); }
"*"								{ printf("lex MUL %s\n", yytext); return yy::parser::make_MUL_OP(yytext); }
">"								{ printf("lex GT %s\n", yytext); return yy::parser::make_GT_OP(yytext); }
"<"								{ printf("lex LT %s\n", yytext); return yy::parser::make_LT_OP(yytext); }
">="							{ printf("lex GTE %s\n", yytext); return yy::parser::make_GTE_OP(yytext); }
"<="							{ printf("lex LTE %s\n", yytext); return yy::parser::make_LTE_OP(yytext); }
"=="							{ printf("lex EQ %s\n", yytext); return yy::parser::make_EQ_OP(yytext); }
"!="							{ printf("lex NEQ %s\n", yytext); return yy::parser::make_NEQ_OP(yytext); }
"&&"							{ printf("lex LAND %s\n", yytext); return yy::parser::make_LAND_OP(SplitGrammar::LAND); }
"||"							{ printf("lex LOR %s\n", yytext); return yy::parser::make_LOR_OP(SplitGrammar::LOR); }
"%"								{ printf("lex MOD %s\n", yytext); return yy::parser::make_MOD_OP(yytext); }
"nonterminal"					{ printf("lex NON_TERM %s\n", yytext); return yy::parser::make_NON_TERM(yytext); }
"terminal"						{ printf("lex TERM %s\n", yytext); return yy::parser::make_TERM(yytext); }
","								{ printf("lex COMMA %s\n", yytext); return yy::parser::make_COMMA(yytext); }
"["|"]"							{ printf("lex IDENT %s\n", yytext); return yy::parser::make_IDENT(yytext); }
"pi"							{ printf("lex pi FP_CONST %s\n", yytext); return yy::parser::make_FP_CONST(3.1415926535897932384626433832795f); }
([a-z]|[A-Z]|_)[a-z|A-Z|0-9|_]* { printf("lex IDENT %s\n", yytext); return yy::parser::make_IDENT(yytext); }
[0-9]*(\.[0-9]+|"")				{ printf("lex FP_CONST %s\n", yytext); return yy::parser::make_FP_CONST((float)atof(yytext)); }
\"[^"]*\"						{ yytext[strlen(yytext) - 1] = 0; printf("lex STR_CONST %s\n", yytext + 1); const char *str = yytext + 1;  return yy::parser::make_STR_CONST(str); }
.								{ printf("lex INVALID CHARACTER %s\n", yytext); }
<<EOF>>							{ printf("lex EOF\n"); return yy::parser::make_EOF(); }
%%