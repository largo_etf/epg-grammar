Relevant source files are parser.y and lexer.l.
C++ source files for lexer and parser are generated using GNU Bison.

Even though engine is developed on Windows, Linux was used to generate parser and lexer code.
For that process Cygwin terminal was used.

https://www.gnu.org/software/bison/
https://www.cygwin.com/