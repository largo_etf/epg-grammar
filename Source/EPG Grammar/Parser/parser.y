%skeleton "lalr1.cc"
%require "3.0.4"
%language "C++"
%define api.value.type variant
%define api.token.constructor
%defines
%debug
%param { SplitGrammar::GrammarParser *grammarParser }

%code requires
{
	#pragma warning(disable : 4065)
	#include <string>
	#include <list>
	#include <iostream>
	#include "GrammarParser.h"
	#include "Symbol.h"
	static void clearFormals();
}

%code
{
	
	#include "Grammar.h"
	#include "Production.h"
	#include "OperationFactory.h"
	#include "BasicSplit.h"
	#include <map>
	#include "Config.h"

	using namespace SplitGrammar;
	using namespace std;

	static Grammar *grammar;

	static SymbolType currType = UNKNOWN;

	static Production *currRule = 0;
	static map<string, int> formals;
	static int formalsCnt = DEFAULT_ATTRIB_CNT;

	//static SymAndArgs *currSymbol = 0;
	static list<ShapeOperation*> shape_op_stack;
	static list<int> remove_flag_stack;
	static int stack_level = 0;
	static Expression *currExpr = 0;
	static int symbolsCnt = 0;
	static int argCnt = 0;
	static int exprTokenCnt = 0;
	//static bool is_basic = false;

	static bool startSet = false;

	yy::parser::symbol_type yylex(GrammarParser *grammarParser);
}

%initial-action
{
	grammar = new Grammar();
}


%define api.token.prefix {TOK_}
%token
	EOF 0	
%token <std::string>
	IDENT 
	PRODUCE
	SEMI
	COLON
	COMMA
	LPAREN
	RPAREN
	LBRACE
	RBRACE
	TERM
	NON_TERM
	ADD_OP
	SUB_OP
	MUL_OP
	DIV_OP
	MOD_OP
	GT_OP
	GTE_OP
	LT_OP
	LTE_OP
	EQ_OP
	NEQ_OP
	REL
%token <SplitGrammar::Code>
	LOR_OP
	LAND_OP
%token <float>
	FP_CONST
%token <char*>
	STR_CONST

%type <int> ShapeOp Rule RuleList Grammar Arg ArgsList ArgsOpt SymbolList SymbolDeclLine SymType DeclList Decl
%type <int> Left LeftName FormalParamsOpt FormalParamList FormalParam
%type <int> Expr TermList Term Factor RelOpt
%type <int> CondExprOpt CondExpr CondTermList CondTerm CondFactorList CondFactor ExprList PiecesOpt Pieces Piece
%type <SplitGrammar::Code> Addop Mulop Relop 
%type <std::string> Ident ArittmeticOp
//%type <std::list<GrammarSymbol*>*> Right;

//%printer { yyoutput << $$; } <*>;

%%

Grammar : SymbolList RuleList
{
	grammarParser->grammar = grammar;
};

// ####################################
// ####################################
// ########## Symbol List #############
// ####################################
// ####################################
SymbolList : SymbolList SymbolDeclLine;
SymbolList : SymbolDeclLine;

SymbolDeclLine : SymType DeclList SEMI;

SymType : NON_TERM 	{ currType = NONTERMINAL; };
SymType : TERM		{ currType = TERMINAL; };

DeclList : DeclList COMMA Decl { $$ = 0; };
DeclList : Decl { $$ = 0; };

Decl : IDENT { grammar->symbols[$1] = new SplitGrammar::Symbol($1, currType); $$ = 0; };

// ####################################
// ####################################
// ########## Rules List ##############
// ####################################
// ####################################
RuleList : RuleList Rule
{

};
RuleList : Rule 
{

};

Rule : Left CondExprOpt PRODUCE Right COLON FP_CONST SEMI 
{
	currRule->probabilty = $6;
	grammar->AddProduction(currRule);

	if (!startSet)
	{
		grammar->startProduction = currRule;
		startSet = true;
	}

	currRule = 0;
};

Left : LeftName FormalParamsOpt { $$ = 0; };

LeftName : IDENT
{
	currRule = new Production();
	currRule->left = grammar->symbols[$1];
	//formals.clear();
	clearFormals();
	formalsCnt = DEFAULT_ATTRIB_CNT;
	symbolsCnt = 0;
	$$ = 0;
};

// ####################################
// ####################################
// ########## Formal Params ###########
// ####################################
// ####################################

FormalParamsOpt : LPAREN FormalParamList RPAREN { $$ = 0; };
FormalParamsOpt : /* epsilon */ { $$ = 0; };

FormalParamList : FormalParamList COMMA FormalParam { $$ = 0; };
FormalParamList : FormalParam { $$ = 0; };

FormalParam : IDENT
{
	formals[$1] = formalsCnt++;
	$$ = 0;
};

// ####################################
// ####################################
// ########## Right Side ##############
// ####################################
// ####################################

Right : Right ShapeOp
{

};

Right : ShapeOp
{

};

ShapeOp : Ident ArgsOpt PiecesOpt
{
	ShapeOperation *op = shape_op_stack.back();
	shape_op_stack.pop_back();

	if (shape_op_stack.size() == 0)
	{
		currRule->rightSide[symbolsCnt++] = op;
	}
	// stack_level--;
	// if (stack_level == 0)
	// {
	// 	while(shape_op_stack.size() != 0)
	// 	{
	// 		currRule->rightSide[symbolsCnt++] = shape_op_stack.front();
	// 		shape_op_stack.pop_front();
	// 	}
	// }
};

PiecesOpt : LBRACE Pieces RBRACE { $$ = 0; };
PiecesOpt : /* epsilon */ { $$ = 0; };

Pieces : Pieces Piece;
Pieces : Piece;

Piece : ShapeOp { $$ = 0; };

Ident : IDENT ArittmeticOp
{
	argCnt = 0;
	string opname = $1 + $2;
	if (grammar->symbols.find(opname) != grammar->symbols.end())
	{
		//is_basic = true;
		BasicSplit *basicSplit = (BasicSplit*)OperationFactory::GetOperation("BasicSplit");
		basicSplit->name = opname;
		basicSplit->symbol = grammar->symbols[opname];
		if (!shape_op_stack.empty())
		{
			shape_op_stack.back()->split_ops[shape_op_stack.back()->subsplits_cnt++] = basicSplit;
		}
		shape_op_stack.push_back(basicSplit);
		stack_level++;
	}
	else
	{
		//is_basic = false;
		ShapeOperation *op = OperationFactory::GetOperation(opname);
		if (op == 0)
			printf("No operation or symbol!\n");
		else
		{
			op->name = opname;
			if (!shape_op_stack.empty())
			{
				shape_op_stack.back()->split_ops[shape_op_stack.back()->subsplits_cnt++] = op;
			}
			shape_op_stack.push_back(op);
			stack_level++;
		}
	}
}

// ####################################
// ####################################
// ########## Real Arguments ##########
// ####################################
// ####################################

ArgsOpt : LPAREN ArgsList RPAREN { $$ = 0; };
ArgsOpt : /* epsilon */ { $$ = 0; };

ArgsList : ArgsList COMMA Arg { $$ = 0; };
ArgsList : Arg { $$ = 0; };

Arg :
{
	currExpr = new Expression();
	shape_op_stack.back()->args[argCnt] = currExpr;
	shape_op_stack.back()->rel_map[argCnt] = 0;
	//currSymbol->args[argCnt] = currExpr;
	exprTokenCnt = 0;
}
Expr RelOpt
{
	argCnt++;
	$$ = 0;
};

RelOpt : REL
{
	shape_op_stack.back()->rel_map[argCnt] = 1;
};

RelOpt : /* epsilon */ { $$ = 0; };

// ####################################
// ####################################
// ########## Expressions #############
// ####################################
// ####################################

Expr : TermList { $$ = 0; };

TermList : TermList Addop Term
{
	currExpr->expr[exprTokenCnt++] = ExprToken($2);
	$$ = 0;
};

TermList : Term { $$ = 0; };

Term : Term Mulop Factor
{
	currExpr->expr[exprTokenCnt++] = ExprToken($2);
	$$ = 0;
};

Term : Factor { $$ = 0 ; };

Factor : FP_CONST
{
	ArgValue token;
	token.floatVal = $1;
	currExpr->expr[exprTokenCnt++] = ExprToken(FP_CONST, token);
	$$ = 0;
};
Factor : IDENT
{
	ArgValue token;
	token.intVal = formals[$1];
	currExpr->expr[exprTokenCnt++] = ExprToken(FP_VAR, token);
	$$ = 0;
};
Factor : STR_CONST
{
	char *str = new char[strlen($1) + 1];
	strcpy(str, $1);
	shape_op_stack.back()->str_arg = str;
	argCnt--;
}
Factor : LPAREN Expr RPAREN { $$ = 0; };

// ####################################
// ####################################
// ############ Condition #############
// ####################################
// ####################################
CondExprOpt :
{ 
	currExpr = new Expression();
	exprTokenCnt = 0;
	currRule->cond = currExpr;
}
COLON CondExpr { $$ = 0; };

CondExprOpt : /* epislon */ { $$ = 0; };

CondExpr : CondTermList { $$ = 0; };

CondTermList : CondTermList LOR_OP CondTerm
{
	currExpr->expr[exprTokenCnt++] = ExprToken($2);
	$$ = 0;
};
CondTermList : CondTerm { $$ = 0; };

CondTerm : CondFactorList { $$ = 0; }

CondFactorList : CondFactorList LAND_OP CondFactor
{
	currExpr->expr[exprTokenCnt++] = ExprToken($2);
	$$ = 0;
};
CondFactorList : CondFactor { $$ = 0; };

CondFactor : ExprList { $$ = 0; };

ExprList : ExprList Relop Expr
{
	currExpr->expr[exprTokenCnt++] = ExprToken($2);
	$$ = 0;
};
ExprList : Expr { $$ = 0; };

// ####################################
// ####################################
// ############ Operators #############
// ####################################
// ####################################

Addop : ADD_OP { $$ = ADD; };
Addop : SUB_OP { $$ = SUB; };

Mulop : MUL_OP { $$ = MUL; };
Mulop : DIV_OP { $$ = DIV; };
Mulop : MOD_OP { $$ = MOD; };

Relop : GT_OP 	{ $$ = GT; };
Relop : GTE_OP 	{ $$ = GTE; };
Relop : LT_OP 	{ $$ = LT; };
Relop : LTE_OP	{ $$ = LTE; };
Relop : EQ_OP	{ $$ = EQ; };
Relop : NEQ_OP	{ $$ = NEQ; };

ArittmeticOp : ADD_OP { $$ = $1; };
ArittmeticOp : SUB_OP { $$ = $1; };
ArittmeticOp : MUL_OP { $$ = $1; };
ArittmeticOp : DIV_OP { $$ = $1; };
ArittmeticOp : MOD_OP { $$ = $1; };
ArittmeticOp : /* epsilon */ { $$ = ""; };

%%

void yy::parser::error(const std::string &m)
{
	std::cout << "Parser error : " << m << std::endl;
}

static void clearFormals()
{
	formals.clear();
	formals["Sx"] = 0;
	formals["Sy"] = 1;
	formals["Sz"] = 2;
}