#ifndef _Bounding_Box_H_
#define _Bounding_Box_H_

#include <glm/glm.hpp>
#include "DSZCore.h"

class Bounding_Box
{
private:
	glm::vec3 p1, p2;
	glm::vec3 *vertices;
	GLuint *indices;
	
	

public:
	Bounding_Box(glm::vec3 p1, glm::vec3 p2);

	glm::vec4 color;

	void Draw();

	bool IntersectsRay(glm::vec3 origin_tr, glm::vec3 direction, float t0, float t1);

	~Bounding_Box();

	friend void DrawBounding_Box(Bounding_Box &box, glm::vec4 color);
};

#endif