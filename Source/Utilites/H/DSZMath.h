#pragma once

#include <glm\mat4x4.hpp>
#include <glm\vec3.hpp>

glm::mat4 axis_to_mat(glm::vec3 x, glm::vec3 y, glm::vec3 z);
void mat_to_axis(glm::mat4 &mat, glm::vec3 &x, glm::vec3 &y, glm::vec3 &z);