#include "DSZMath.h"

using namespace glm;

glm::mat4 axis_to_mat(glm::vec3 x, glm::vec3 y, glm::vec3 z)
{
	mat4 mat;
	memcpy(&(mat[0][0]), &x, 3 * sizeof(float));
	memcpy(&(mat[1][0]), &y, 3 * sizeof(float));
	memcpy(&(mat[2][0]), &z, 3 * sizeof(float));

	return mat;
}

void mat_to_axis(mat4 &mat, vec3 &x, vec3 &y, vec3 &z)
{
	memcpy(&x, &(mat[0][0]), 3 * sizeof(float));
	memcpy(&y, &(mat[1][0]), 3 * sizeof(float));
	memcpy(&z, &(mat[2][0]), 3 * sizeof(float));
}