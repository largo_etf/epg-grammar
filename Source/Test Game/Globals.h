#pragma once

#include <Windows.h>

class Game;
class ShaderProgram;

extern Game *game;
extern ShaderProgram SimpleShader;

extern HINSTANCE hInstance;

extern bool fullscreen;

extern int _width;
extern int _height;
