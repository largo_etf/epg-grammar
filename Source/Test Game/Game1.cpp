#include "Game1.h"
#include "GameTime.h"
#include "Grid.h"
#include "Globals.h"
#include "Camera.h"
#include "DSZMathHeader.h"
#include "Mesh.h"
#include "Model.h"
#include "DSZMaterial.h"
#include "GrammarParser.h"
#include "Grammar.h"
#include "PNGTexture.h"
#include "GeometryBatch.h"
#include "OperationFactory.h"
#include "HalfOp.h"
using namespace glm;
using namespace SplitGrammar;

static Grid *grid;
static Camera *camera;
static Model *model;
static Mesh *cubeMesh;
static GrammarModel *grModel = 0;
static Grammar *grammar;

#define CITY_X 1
#define CITY_Z 1

static GrammarModel *city[CITY_X][CITY_Z];
struct xz
{
	float x, z;
};

static struct xz city_dims[CITY_X][CITY_Z];

static bool city_initialized = false;

#define BSIZE 2
static GeometryBatch batches[BSIZE];

float w = 5.0f, h, l;
float x, z;

float total_w, total_l;

void Game1::Update(GameTime gameTime)
{
	camera->Update(gameTime);

	if (Input::GetScrollValue() != 0 && Input::GetButtonState(RIGHT) == DOWN)
	{
		x += Input::GetScrollValue() / 40.0f;
		printf("x = %f\n", x);
	}

	if (Input::GetScrollValue() != 0 && Input::GetButtonState(LEFT) == DOWN)
	{
		z += Input::GetScrollValue() / 40.0f;
		printf("z = %f\n", z);
	}

	if ((Input::IsMouseButtonPressed(LEFT) || (Input::GetScrollValue() != 0 && Input::GetButtonState(RIGHT) == DOWN)))
	{
		city_initialized = true;
		total_w = 0;
		total_l = 0;
		for (int x = 0; x < CITY_X; ++x)
		{
			float curr_w = 0, curr_l = 0;
			for (int z = 0; z < CITY_Z; ++z)
			{
				delete city[x][z];
				float args[MAX_ARGS];
				memset(args, 0, MAX_ARGS * sizeof(float));

				w = ((float)rand() / (float)RAND_MAX) * 20 + 15;
				h = ((float)rand() / (float)RAND_MAX) * 8 * 5 + 13;
				l = ((float)rand() / (float)RAND_MAX) * 20 + 15;
				args[0] = 3;
				args[1] = 6;
				args[2] = 10;
				//args[0] = 2.0f;
				//args[1] = 10.0f;
				//args[2] = 2.0f;
				city[x][z] = grammar->Generate(args);
				xz dims;
				dims.x = w;
				dims.z = l;
				city_dims[x][z] = dims;
				curr_w += w;
				curr_l += l;
			}

			total_w = curr_w > total_w ? curr_w : total_w;
			total_l = curr_l > total_l ? curr_l : total_l;
		}
	}
}

void Game1::Draw()
{
	SimpleShader.SetAttribute("View", camera->GetViewMatrix());
	SimpleShader.SetAttribute("Projection", camera->GetProjection());

	grid->draw();

	if (city_initialized)
	{
		float move_x = 0, move_z = 0;
		for (int x = 0; x < CITY_X; ++x)
		{
			for (int z = 0; z < CITY_Z; ++z)
			{
				mat4 world;
				//world *= translate(vec3((x - CITY_X/2) * 27, 0, (z - CITY_Z/2) * 27));

				//world *= translate(vec3(move_x, 0, move_z));
				//world *= translate(-vec3(total_w / 2, 0, total_l / 2));
				city[x][z]->Draw(world);

				move_x += city_dims[x][z].x;
				if (x > 0)
					move_z = city_dims[x-1][z].z;
			}
		}
	}

}

#include "DSZMath.h"

void Game1::Initialize()
{
	grid = new Grid(20);
	camera = new Camera(45.0f, ((float)Width()/Height()), 0.1f, 100000.0f, 22, vec3(0, 0, 0), (float)(3 * M_PI / 4), -(float)(M_PI / 6.0f));
	model = new Model();
	model->Load("..\\Resources\\kocka.FBX");

	cubeMesh = new Mesh();
	cubeMesh->GenerateCube();
	
	DSZMaterial *mat = new DSZMaterial(&SimpleShader);
	mat->color = vec4(0.0, 1.0, 0.0, 1.0);
	mat->UseStaticColor();
	mat->UseLighting = true;
	cubeMesh->material = mat;
	
	HalfOp *half_op_prot = new HalfOp();
	OperationFactory::RegisterOperation(half_op_prot, "Half");
	GrammarParser grparser;
	grammar = grparser.Parse("nbg.sg");
}

void Game1::LoadContent()
{
	SimpleShader.InstallShader("..\\Source\\vs1.vert", "..\\Source\\fs1.frag");
}

void Game1::UnloadContent()
{

}

Game1::~Game1()
{
	delete grid;
	delete camera;
}