#pragma once

#include <map>
#include <string>

class Material
{
private:
	static int idGen;

protected:
	int id;
public:
	Material();

	virtual void SetAttributes() = 0;
};