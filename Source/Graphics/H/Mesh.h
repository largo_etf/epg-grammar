#pragma once

#include "DSZCore.h"
#include <fbxsdk\scene\geometry\fbxmesh.h>
#include "Material.h"
#include <glm\mat4x4.hpp>

class Model;

class Mesh
{
private:
	GLuint vao, vertexBuffer, indexBuffer, normalsBuffer, colorsBuffer, uvBuffer;
	GLfloat *vertices, *normals, *colors, *uvCoordinates;
	int *indices;

	int numIndices, numVertices, numNormals;

	Model* model;

	void loadVertices(fbxsdk::FbxMesh* mesh, fbxsdk::FbxMatrix& transform);
	void loadIndices(fbxsdk::FbxMesh* mesh);
	void loadNormals(fbxsdk::FbxMesh* mesh, fbxsdk::FbxMatrix& transform);
	void loadColors(fbxsdk::FbxMesh* mesh);
	void loadUVCoordinates(fbxsdk::FbxMesh *mesh);
public:
	Mesh();

	Material *material;

	bool Load(fbxsdk::FbxMesh* meshNode);
	bool Unload();

	void GenerateCube();

	void GetMinMaxVectors(glm::vec3 &min, glm::vec3 &max) const;

	void Draw(glm::mat4 worldTransform);

	~Mesh();

	friend class GeometryBatch;
};