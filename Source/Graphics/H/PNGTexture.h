#ifndef _PNGTEXTURE_H_
#define _PNGTEXTURE_H_

#include "Texture.h"
#include <png.h>

class PNGTexture : public Texture
{
private:
	int width, height;
public:
	virtual void Load(const char * file_name);

	virtual ~PNGTexture();
};

#endif