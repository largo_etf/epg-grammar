#pragma once

#include "DSZCore.h"
#include <png.h>
#include <map>
#include <string>

class Texture
{
protected:
	GLuint texture = 0;
	GLuint sampler = 0;

	static std::map<std::string, Texture*> tex_cache;
public:
	virtual void Load(const char * file_name) = 0;
	static Texture* GetTexture(const char *file_name);
	void Activate(GLuint textureUnit);

	virtual ~Texture();
};
