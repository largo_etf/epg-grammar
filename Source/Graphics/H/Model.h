#pragma once

#include "DSZCore.h"
#include "Mesh.h"
#include <list>
#include <glm\mat4x4.hpp>
#include <map>
#include <string>

class Model
{
private:
	

	static std::map<std::string, Model*> models;

	void TraverseNodes(fbxsdk::FbxNode *root);

public:
	void Init();

	std::list<Mesh*> meshes;

	bool Load(const char *fileName);
	static Model *GetModel(const char *fileName);

	void AddMesh(Mesh *mesh);

	void SetAllMeshesMaterial(Material *mat);

	void Draw(glm::mat4 worldTransform);

	void GetMinMaxVectors(glm::vec3 &min, glm::vec3 &max);

	~Model();
};