#pragma once

#include "Material.h"
#include "Texture.h"
#include "DSZMathHeader.h"

class DSZMaterial : public Material
{
private:
	typedef enum Coloring { TEXTURE, COLOR, DEFAULT };
	Coloring Coloring = DEFAULT;

public:
	DSZMaterial(ShaderProgram *shader);

	Texture *Texture;
	ShaderProgram *Shader;
	glm::vec4 color = glm::vec4(0, 0, 0, 1);
	glm::vec4 TintColor = glm::vec4(0, 0, 0, 1);

	bool UseLighting = true;

	void UseTexture();
	void UseStaticColor();
	void UseDefaultColors();

	virtual void SetAttributes();
};