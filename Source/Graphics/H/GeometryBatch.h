#pragma once

#include <map>
#include <vector>
#include <glm\mat4x4.hpp>
#include <glew.h>
#include <string>

class Model;
class DSZMaterial;
class Material;

namespace SplitGrammar
{
	struct GrammarModel;
}

class InstanceAttribs
{
public:
	InstanceAttribs();

	std::vector<glm::mat4> transforms;
	GLuint gl_buffer;
};

class BatchKey
{
public:
	BatchKey(std::string material_name, Model* model);
	std::string material_name;
	Model *model;

	bool operator<(const BatchKey &key) const;
};

class GeometryBatch
{
private:
	std::map<BatchKey, InstanceAttribs> instances;

public:
	void AddInstance(BatchKey model, glm::mat4 transform);

	void Draw(glm::mat4 world, SplitGrammar::GrammarModel* grammar_model);
};