#include "GeometryBatch.h"
#include "Model.h"
#include "ShaderProgram.h"
#include "Grammar.h"

using namespace std;
using namespace glm;
using namespace SplitGrammar;

void GeometryBatch::AddInstance(BatchKey key, glm::mat4 transform)
{
		//for (std::list<Mesh*>::iterator it = model->meshes.begin(); it != model->meshes.end(); it++)
		//{
		//	glBindVertexArray((*it)->vao);
		//	glBindBuffer(GL_ARRAY_BUFFER, instances[model].gl_buffer);

		//	GLuint location = 4;
		//	GLint components = 4;
		//	GLenum type = GL_FLOAT;
		//	GLboolean normalized = GL_FALSE;
		//	GLsizei datasize = sizeof(mat4);
		//	char* pointer = 0;
		//	GLuint divisor = 1;

		//	for (int c = 0; c < 4; ++c)
		//	{
		//		glEnableVertexAttribArray(location + c); //location of each column
		//		glVertexAttribPointer(location + c, components, type, normalized, datasize, pointer + c * sizeof(vec4)); //tell other data
		//		glVertexAttribDivisor(location + c, divisor); //is it instanced?
		//	}
		//}

	instances[key].transforms.push_back(transform);
}

void GeometryBatch::Draw(mat4 world, GrammarModel *grammar_model)
{
	SimpleShader.SetAttribute("World", world);
	SimpleShader.SetAttribute("instancing", true);

	for (map<BatchKey, InstanceAttribs>::iterator it = instances.begin(); it != instances.end(); it++)
	{
		Model *model = it->first.model;
		Material *mat = grammar_model->GetMaterial(it->first.material_name);
		InstanceAttribs &attribs = it->second;

		/*(*it).first->Draw((*it).second.transforms[i]);*/
		for (std::list<Mesh*>::iterator it = model->meshes.begin(); it != model->meshes.end(); it++)
		{
			Mesh *mesh = *it;
			//mesh->material->SetAttributes();
			//(*it)->Draw(worldTransform);
			mat->SetAttributes();

			glBindVertexArray(mesh->vao);

			glBindBuffer(GL_ARRAY_BUFFER, attribs.gl_buffer);
			GLuint location = 4;
			GLint components = 4;
			GLenum type = GL_FLOAT;
			GLboolean normalized = GL_FALSE;
			GLsizei datasize = sizeof(mat4);
			char* pointer = 0;
			GLuint divisor = 1;

			for (int c = 0; c < 4; ++c)
			{
				glEnableVertexAttribArray(location + c); //location of each column
				glVertexAttribPointer(location + c, components, type, normalized, datasize, pointer + c * sizeof(vec4)); //tell other data
				glVertexAttribDivisor(location + c, divisor); //is it instanced?
			}
		
			glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * attribs.transforms.size(), &(attribs.transforms[0]), GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);

			glDrawElementsInstanced(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, 0, attribs.transforms.size());

			
		}

	}
}

InstanceAttribs::InstanceAttribs()
{
	glGenBuffers(1, &gl_buffer);
	//glBindBuffer(GL_ARRAY_BUFFER, gl_buffer);

}

BatchKey::BatchKey(std::string material_name, Model* model)
	: material_name(material_name), model(model)
{

}

bool BatchKey::operator<(const BatchKey &key) const
{
	string key1, key2;
	key1 = material_name + std::to_string((int)model);
	key2 = key.material_name + std::to_string((int)key.model);
	return key1 < key2;
}