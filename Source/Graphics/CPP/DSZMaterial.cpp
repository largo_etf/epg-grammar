#include "DSZMaterial.h"
#include "ShaderProgram.h"

DSZMaterial::DSZMaterial(ShaderProgram *shader)
	:Shader(shader)
{

}

void DSZMaterial::SetAttributes()
{
	if (Shader != NULL)
	{
		Shader->Activate();
		Shader->SetAttribute("UseLighting", UseLighting);
		Shader->SetAttribute("TintColor", TintColor);
		switch (Coloring)
		{
		case TEXTURE:
			Shader->SetAttribute("UseTexture", true);
			Shader->SetAttribute("UseStaticColor", false);
			Shader->SetAttribute("TexSampler", 0);
			if (this->Texture != NULL)
				this->Texture->Activate(0);
			break;

		case COLOR:
			Shader->SetAttribute("UseTexture", false);
			Shader->SetAttribute("UseStaticColor", true);
			Shader->SetAttribute("StaticColor", this->color);
			break;

		case DEFAULT:
			Shader->SetAttribute("UseTexture", false);
			Shader->SetAttribute("UseStaticColor", false);
			break;
		}
	}
}

void DSZMaterial::UseTexture()
{
	this->Coloring = TEXTURE;
}

void DSZMaterial::UseStaticColor()
{
	this->Coloring = COLOR;
}

void DSZMaterial::UseDefaultColors()
{
	this->Coloring = DEFAULT;
}