#include "Texture.h"

void Texture::Activate(GLuint unit)
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, texture);
}

Texture* Texture::GetTexture(const char *file_name)
{
	if (tex_cache.find(file_name) != tex_cache.end())
		return tex_cache[file_name];
	else
		return 0;
}

Texture::~Texture()
{
	glDeleteTextures(1, &texture);
}

std::map<std::string, Texture*> Texture::tex_cache;