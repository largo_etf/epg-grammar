#include "Mesh.h"
#include <fbxsdk.h>
#include <iostream>
#include "DSZMathHeader.h"
#include "DSZCore.h"
#include "ShaderProgram.h"
//#include "PNGTexture.h"

using namespace fbxsdk;
using namespace glm;

Mesh::Mesh()
	: material(0)
{

}

glm::mat4x4 FbxMatToGlm(FbxMatrix &m)
{
	glm::mat4x4 mat;
	mat *= glm::translate(glm::vec3(2, 2, 2));

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			mat[i][j] = (float)m[i][j];


	return mat;
}

glm::vec4 FbxVecToGlm(FbxVector4 &v)
{
	glm::vec4 vec;
	for (int i = 0; i < 4; i++)
		vec[i] = (float)v[i];

	return vec;
}

void Mesh::loadVertices(FbxMesh* mesh, FbxMatrix& transform)
{
	numVertices = mesh->GetControlPointsCount();
	vertices = new GLfloat[numVertices * 3];
	for (int i = 0; i < numVertices; i++)
	{
		FbxVector4 vec = transform.MultNormalize(mesh->GetControlPointAt(i));

		vertices[i * 3 + 0] = (GLfloat)vec.mData[0];
		vertices[i * 3 + 1] = (GLfloat)vec.mData[1];
		vertices[i * 3 + 2] = (GLfloat)vec.mData[2];
	}

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * numVertices, vertices, GL_STATIC_DRAW);
}

void Mesh::loadIndices(FbxMesh* mesh)
{
	numIndices = mesh->GetPolygonVertexCount();
	int* tempIndices = mesh->GetPolygonVertices();
	indices = new int[numIndices];

	for (int i = 0; i < numIndices; i++)
		indices[i] = tempIndices[i];

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)* numIndices, indices, GL_STATIC_DRAW);
}

void Mesh::loadNormals(FbxMesh* mesh, FbxMatrix& transform)
{
	FbxGeometryElementNormal *normalElement = mesh->GetElementNormal();
	if (normalElement)
	{
		glm::mat4x4 glmMat = FbxMatToGlm(transform);

		numNormals = mesh->GetPolygonCount() * 3;
		normals = new GLfloat[numNormals * 3];

		int vertexCounter = 0;

		for (int polyCounter = 0; polyCounter < mesh->GetPolygonCount(); polyCounter++)
		{
			for (int i = 0; i < 3; i++)
			{
				FbxVector4 normal = normalElement->GetDirectArray().GetAt(vertexCounter);

				glm::vec4 glmNormal = FbxVecToGlm(normal);
				glmNormal = glmMat * glmNormal;

				normals[vertexCounter * 3 + 0] = (GLfloat)glmNormal[0];
				normals[vertexCounter * 3 + 1] = (GLfloat)glmNormal[1];
				normals[vertexCounter * 3 + 2] = (GLfloat)glmNormal[2];

				vertexCounter++;
			}
		}

		glGenBuffers(1, &normalsBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * numNormals, normals, GL_STATIC_DRAW);
	}
}

void Mesh::loadColors(FbxMesh* mesh)
{
	FbxLayer *layer = mesh->GetLayer(0);
	FbxLayerElementMaterial* pMaterialLayer = layer->GetMaterials();
	int numMatIndices = pMaterialLayer->GetIndexArray().GetCount();

	colors = new GLfloat[numVertices * 3];
	FbxLayerElementMaterial::EMappingMode mm = pMaterialLayer->GetMappingMode();

	if (mm = FbxLayerElementMaterial::eByPolygon)
	{
		for (int i = 0; i < numMatIndices; i++)
		{
			int matIndex = pMaterialLayer->GetIndexArray()[i];

			FbxSurfaceMaterial *smat = mesh->GetNode()->GetMaterial(matIndex);
			if (smat != NULL && smat->GetClassId().Is(FbxSurfaceLambert::ClassId))
			{
				FbxSurfaceLambert *lam = (FbxSurfaceLambert*)smat;

				FbxPropertyT<FbxDouble3> p = lam->Diffuse;
				FbxDouble3 info = p.Get();

				for (int j = 0; j < 3; j++)
				{
					int pos = indices[i * 3 + j];
					colors[pos * 3 + 0] = (GLfloat)info[0];
					colors[pos * 3 + 1] = (GLfloat)info[1];
					colors[pos * 3 + 2] = (GLfloat)info[2];
				}
			}
		}
	}
	else if (mm == FbxLayerElementMaterial::eAllSame)
	{
		FbxSurfaceMaterial *smat = mesh->GetNode()->GetMaterial(0);

		if (smat != NULL && smat->GetClassId().Is(FbxSurfaceLambert::ClassId))
		{
			FbxSurfaceLambert *lam = (FbxSurfaceLambert*)smat;

			FbxPropertyT<FbxDouble3> p = lam->Diffuse;
			FbxDouble3 info = p.Get();

			for (int i = 0; i < numVertices; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					colors[i * 3 + 0] = (GLfloat)info[0];
					colors[i * 3 + 1] = (GLfloat)info[1];
					colors[i * 3 + 2] = (GLfloat)info[2];
				}
			}
		}
	}

	glGenBuffers(1, &colorsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 9 * numMatIndices, colors, GL_STATIC_DRAW);
}

void Mesh::loadUVCoordinates(FbxMesh *mesh)
{
	FbxStringList uvSetNameList;
	mesh->GetUVSetNames(uvSetNameList);

	uvCoordinates = (GLfloat*)malloc(0);
	int uvCount = 0;

	for (int uvSetIndex = 0; uvSetIndex < uvSetNameList.GetCount(); uvSetIndex++)
	{
		const char* uvSetName = uvSetNameList.GetStringAt(uvSetIndex);
		const FbxGeometryElementUV *uvElement = mesh->GetElementUV(uvSetName);

		if (!uvElement)
			continue;

		if (uvElement->GetMappingMode() != FbxGeometryElement::eByPolygonVertex &&
			uvElement->GetMappingMode() != FbxGeometryElement::eByControlPoint)
			return;

		const bool useIndex = uvElement->GetReferenceMode() != FbxGeometryElement::eDirect;
		const int indexCount = (useIndex) ? uvElement->GetIndexArray().GetCount() : 0;

		const int polyCount = mesh->GetPolygonCount();
		const int polySize = mesh->GetPolygonSize(0);
		const int cnt = polyCount*polySize * 2;
		uvCoordinates = (GLfloat*)realloc(uvCoordinates, sizeof(GLfloat)*polyCount*polySize * 2);

		if (uvElement->GetMappingMode() == FbxGeometryElement::eByControlPoint)
		{
			for (int polyIndex = 0; polyIndex < polyCount; polyIndex++)
			{
				for (int vertIndex = 0; vertIndex < polySize; vertIndex++)
				{
					FbxVector2 uv;

					int polyVertIndex = mesh->GetPolygonVertex(polyIndex, vertIndex);

					int uvIndex = useIndex ? uvElement->GetIndexArray().GetAt(polyVertIndex) : polyVertIndex;

					uv = uvElement->GetDirectArray().GetAt(uvIndex);
					int pos = indices[polyIndex*polySize + vertIndex];
					uvCoordinates[pos * 2 + 0] = (float)uv.mData[0];
					uvCoordinates[pos * 2 + 1] = (float)uv.mData[1];
				}
			}
		}

		uvCount = cnt;
	}

	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* uvCount, uvCoordinates, GL_STATIC_DRAW);
}

GLuint png_texture_load(const char * file_name, int * width, int * height);

bool Mesh::Load(FbxMesh *mesh)
{
	FbxScene *scene = mesh->GetScene();
	FbxAnimEvaluator *eval = scene->GetAnimationEvaluator();
	FbxMatrix transform = eval->GetNodeGlobalTransform(mesh->GetNode());

	loadVertices(mesh, transform);

	loadIndices(mesh);

	loadNormals(mesh, transform);

	loadColors(mesh);

	loadUVCoordinates(mesh);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	return true;
}

void Mesh::Draw(glm::mat4 worldTransform)
{
	if (this->material != NULL)
		material->SetAttributes();
	//SimpleShader.Activate();
	SimpleShader.SetAttribute("World", worldTransform);

	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);
}

bool Mesh::Unload()
{
	delete[] vertices;
	delete[] normals;
	delete[] colors;
	delete[] indices;
	delete[] uvCoordinates;

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &indexBuffer);
	glDeleteBuffers(1, &normalsBuffer);
	glDeleteBuffers(1, &colorsBuffer);
	glDeleteBuffers(1, &uvBuffer);

	return true;
}

void Mesh::GetMinMaxVectors(glm::vec3 &min, glm::vec3 &max) const
{
	GLfloat minx = vertices[0], miny = vertices[1], minz = vertices[2];
	GLfloat maxx = vertices[0], maxy = vertices[1], maxz = vertices[2];

	for (int i = 0; i < numVertices; i++)
	{
		minx = glm::min(minx, vertices[i * 3 + 0]);
		miny = glm::min(miny, vertices[i * 3 + 1]);
		minz = glm::min(minz, vertices[i * 3 + 2]);

		maxx = glm::max(maxx, vertices[i * 3 + 0]);
		maxy = glm::max(maxy, vertices[i * 3 + 1]);
		maxz = glm::max(maxz, vertices[i * 3 + 2]);
	}

	min = vec3(minx, miny, minz);
	max = vec3(maxx, maxy, maxz);;
}

Mesh::~Mesh()
{
	Unload();
}

void Mesh::GenerateCube()
{
	float p = 1.0f;
	this->numVertices = 8;
	this->numNormals = 8;
	this->numIndices = 36;
	vec3 pointss[] =
	{
		vec3(0, p, p),
		vec3(0, 0, p),
		vec3(p, 0, p),
		vec3(p, p, p),
		vec3(0, p, 0),
		vec3(0, 0, 0),
		vec3(p, 0, 0),
		vec3(p, p, 0)
	};
	this->vertices = new GLfloat[24];
	memcpy(this->vertices, pointss, 8 * sizeof(vec3));

	vec3 normalss[] =
	{
		vec3(0, p, p),
		vec3(0, 0, p),
		vec3(p, 0, p),
		vec3(p, p, p),
		vec3(0, p, 0),
		vec3(0, 0, 0),
		vec3(p, 0, 0),
		vec3(p, p, 0)
	};
	this->normals = new GLfloat[8 * sizeof(vec3)];
	memcpy(this->normals, normalss, 8 * sizeof(vec3));

	int indicess[] =
	{
		0, 1, 2,
		0, 2, 3,
		2, 6, 3,
		3, 6, 7,
		7, 6, 5,
		7, 5, 4,
		0, 4, 5,
		0, 5, 1,
		4, 0, 3,
		4, 3, 7,
		1, 5, 2,
		2, 5, 6
	};
	this->indices = new int[36];
	memcpy(indices, indicess, 36 * sizeof(int));

	vec3 colourr[] =
	{
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
		vec3(0.5f, 0.5f, 0.5f),
	};
	this->colors = new GLfloat[8 * sizeof(vec3)];
	memcpy(colors, colourr, 8 * sizeof(vec3));

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(vec3), pointss, GL_STATIC_DRAW);

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(int), indicess, GL_STATIC_DRAW);

	glGenBuffers(1, &colorsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(vec3), colourr, GL_STATIC_DRAW);

	glGenBuffers(1, &normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(vec3), normalss, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}