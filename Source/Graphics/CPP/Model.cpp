#include "Model.h"
#include <iostream>
#include <fbxsdk.h>
#include "DSZMathHeader.h"
#include "ShaderProgram.h"
#include "HelperFunctions.h"

using namespace glm;
using namespace fbxsdk;

#define DEBUG 1

std::map<std::string, Model*> Model::models;

void Model::Init()
{

}

bool Model::Load(const char* fileName)
{
	FbxManager *manager = FbxManager::Create();
	FbxIOSettings *ios = FbxIOSettings::Create(manager, IOSROOT);
	FbxImporter *importer = FbxImporter::Create(manager, "DSZ Importer");

	if (!importer->Initialize(fileName, -1, manager->GetIOSettings()))
	{
		std::cout << "FBXImporter initialization falied!\n" << std::endl;
		importer->Destroy();
		manager->Destroy();
		return false;
	}

	FbxScene *scene = FbxScene::Create(manager, "DSZ Scena");
	importer->Import(scene);

	FbxNode *root = scene->GetRootNode();

	TraverseNodes(root);

	manager->Destroy();

	return true;
}

Model *Model::GetModel(const char *name)
{
	if (models.find(std::string(name)) != models.end())
		return models[std::string(name)];

	Model *model = new Model();
	model->Load(name);
	models[std::string(name)] = model;
	return model;
}

void Model::AddMesh(Mesh *mesh)
{
	this->meshes.push_back(mesh);
}

void Model::TraverseNodes(FbxNode *node)
{
	int numChildren = node->GetChildCount();
	FbxNode *childNode = 0;

	for (int i = 0; i < numChildren; i++)
	{
		childNode = node->GetChild(i);

		FbxMesh *meshNode = childNode->GetMesh();

		if (meshNode != NULL)
		{
			Mesh* mesh = new Mesh();
			this->meshes.push_front(mesh);

			mesh->Load(meshNode);
		}

		TraverseNodes(childNode);
	}
}

void Model::SetAllMeshesMaterial(Material *mat)
{
	for (std::list<Mesh*>::iterator it = meshes.begin(); it != meshes.end(); it++)
		(*it)->material = mat;
}

void Model::Draw(glm::mat4 worldTransform)
{
	if (DEBUG)
	{
		DrawLine(vec3(0, 0, 0), vec3(3, 0, 0), vec4(1, 0, 0, 1), worldTransform);
		DrawLine(vec3(0, 0, 0), vec3(0, 3, 0), vec4(1, 1, 0, 1), worldTransform);
		DrawLine(vec3(0, 0, 0), vec3(0, 0, 3), vec4(0, 0, 1, 1), worldTransform);
	}
	for (std::list<Mesh*>::iterator it = meshes.begin(); it != meshes.end(); it++)
		(*it)->Draw(worldTransform);
}

void Model::GetMinMaxVectors(glm::vec3 &min, glm::vec3 &max)
{
	vec3 _min, _max;
	vec3 tmin, tmax;

	meshes.begin()._Mynode()->_Myval->GetMinMaxVectors(_min, _max);

	for (std::list<Mesh*>::iterator it = meshes.begin(); it != meshes.end(); it++)
	{
		it._Mynode()->_Myval->GetMinMaxVectors(tmin, tmax);

		_min.x = glm::min(_min.x, tmin.x);
		_min.y = glm::min(_min.y, tmin.y);
		_min.z = glm::min(_min.z, tmin.z);

		_max.x = glm::max(_max.x, tmax.x);
		_max.y = glm::max(_max.y, tmax.y);
		_max.z = glm::max(_max.z, tmax.z);
	}

	min = _min;
	max = _max;
}

Model::~Model()
{
	for (std::list<Mesh*>::iterator it = meshes.begin(); it != meshes.end(); it++)
		delete it._Mynode()->_Myval;

	meshes.clear();
}